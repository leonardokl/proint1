<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 25/05/2015
 * Time: 16:58
 */

	namespace App\Model\Table;

    use Cake\ORM\Table;

    class DescriptionsTable extends Table
    {
        public function initialize(array $config)
        {
            $this->belongsTo('Users');
        }
    }
    ?>