<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 28/03/2015
 * Time: 23:47
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProjectsTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasOne('Searches');
        $this->belongsTo('Areas');
        $this->belongsTo('Status');
        $this->belongsTo('Campus');
        $this->belongsTo('IntellectualProperties');
        $this->belongsTo('Users');
        $this->hasMany('Members');
    }

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('name', 'A name is required')
            ->notEmpty('area_id', 'A area is required');
    }

}
?>