<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 29/05/2015
 * Time: 22:31
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MembersTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Projects');
        $this->belongsTo('MembersStatus');
        $this->belongsTo('Campus');
    }
}
?>