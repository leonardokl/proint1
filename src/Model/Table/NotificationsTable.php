<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 06/04/2015
 * Time: 21:54
 */

	namespace App\Model\Table;

    use Cake\ORM\Table;

    class NotificationsTable extends Table
    {
        public function initialize(array $config)
        {
            $this->belongsTo('Users');
        }
    }
?>