<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 28/03/2015
 * Time: 23:47
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SearchesTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Projects');
        $this->belongsTo('Users');
        $this->belongsTo('Status');
        $this->hasOne('Searches');
        $this->hasMany('Descriptions');
    }
}
?>