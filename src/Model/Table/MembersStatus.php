<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 29/05/2015
 * Time: 22:36
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MembersStatusTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasOne('Members');
    }
}
?>