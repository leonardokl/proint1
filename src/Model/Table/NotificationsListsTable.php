<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 09/04/2015
 * Time: 15:03
 */

namespace App\Model\Table;

use Cake\ORM\Table;

class NotificationsListsTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Users');
    }

    public function add($id)
    {
        $notifications_lists = $this->NotificationsLists->newEntity();


        $notifications_lists = $this->NotificationsLists->patchEntity($notifications_lists, $this->request->data);
        $notifications_lists->user_id = $id;

        if ($this->NotificationsLists->save($notifications_lists)) {
            $this->Flash->success(__('Projeto cadastrado.'));

        }

        $this->Flash->error(__('Unable to add the project.'));
    }

}
?>