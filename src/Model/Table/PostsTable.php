<?php

	namespace App\Model\Table;

	use Cake\ORM\Table;

	class PostsTable extends Table
	{
		public function initialize(array $config){
			$this->addBehavior('Timestamp');//preenchimeto automático datas
			$this->displayField('title');//combobox
		}
	}


?>