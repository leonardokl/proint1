<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 06/04/2015
 * Time: 21:48
 */
namespace App\Controller;

use Cake\Network\Email\Email;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class NotificationsController extends AppController
{

    public function index(){
        $this->set('notifications', $this->Notifications->find('all')->where(['user_id' => $this->Auth->user('id')])->order(['Notifications.id' => 'DESC']));
    }

    public function delete($id){
        $notification = $this->Notifications->get($id);
        if ($this->Notifications->delete($notification)) {
            $this->Flash->success(__('The notification with id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function view($id)
    {
        $notification = $this->Notifications->get($id);
        $this->set(compact('notification'));

        if($notification->seen == 0){
            $notification->seen = 1;
            if ($this->Notifications->save($notification)){
                1==1;
            }
        }
    }

    public function ajax() {

    }

    public function ajaxMsg(){
        $this->layout = false;
        //aqui poderiamos ter, requisições a banco de dados //validações, chamada à outas DataSources etc.
        $this->set("mensagem","Olá Mundo! CakePHP Ajax");
    }

    public function email()
    {
        //mail('leonardokl@hotmail.com', 'Sample mail', 'Sample content', 'From: proint20151@gmail.com');
        $this->Email->smtpOptions = array(
            'timeout'=> '5',
            'host' => 'ssl://in.mailjet.com',
            'port'=> '465',
            'username'=>'618b35be52452abe0f3096141c6acd4e',
            'password'=>'16381146e92f033a356e3c3d37649282');

        $this->Email->delivery = 'smtp';

        $this->Email->from = 'proint20151@gmail.com';
        $this->Email->to = 'leonardokl@hotmail.com';
        $this->Email->subject = 'My first email by Mailjet';

        $this->Email->send('Hello from Mailjet & CakePHP 1.3 !');
    }


}
