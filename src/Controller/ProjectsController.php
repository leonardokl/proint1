<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\AreasTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class ProjectsController extends AppController
{

    public $components = array('Paginator');
    public $paginate = array(
            'limit' => 10,            
    );
    
    public function index($status = NULL, $busca = NULL)
    {

        if ($this->request->is('post')) {
            $busca = $this->request->data["busca"];
            $this->set( 'projects', $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status'])->where(['title LIKE' => '%'. $busca .'%']) ) );
        }
        //BUSCAS POR STATUS DO PROJETO
        else if ($status!=NULL) {
            $this->set( 'projects', $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status'])->where(['status_id' => $status])->order(['Projects.id' => 'DESC']) )  );
        } else {
            $this->set( 'projects', $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status'])->order(['Projects.id' => 'DESC']) ) );
        }
    }

    public function status($status)
    {
        $this->set( 'projects', $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status'])->where(['status_id' => $status]) )  );
    }

    public function noSearch()
    {
        $this->set( 'projects', $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status'])->where(['status_id' => 4]) )  );
    }

    public function view($id)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid project'));
        }
        $this->loadModel('Members');
        $this->loadModel('MembersStatus');
        $this->set('members', $this->Members->find('all')->contain(['MembersStatus'])->where(['Members.project_id' => $id]));
        $this->set( 'projects', $projects = $this->paginate( $this->Projects->find( )->contain(['Areas', 'Status', 'Campus', 'IntellectualProperties'])->where(['projects.id' => $id]) ) );

        //GAMBIARRA
        $projeto_id = 0;
        foreach($projects as $project){
            $projeto_id = $project->id;
        }
        $this->loadModel('Searches');
        $buscas =  $this->Searches->find()->contain(['Users'])->where(['Searches.project_id' => $projeto_id]);
        $result = 0;
        foreach ($buscas as $busca) {
            $result = $busca;
        }
        if($result->id){
            $this->set('busca', $result);
        }else{
            $this->set('busca', NULL);
        }

        //$this->set( 'project', $project = $this->Projects->get($id));
    }

    public function add()
    {
        $this->loadModel('Members');
        $this->loadModel('MembersStatus');

        $project = $this->Projects->newEntity();
        $member = $this->Members->newEntity();

        if ($this->request->is('post')) {
            $project = $this->Projects->patchEntity($project, $this->request->data);
            $project->user_id = $this->Auth->user('id');

            if ($this->Projects->save($project)) {
                $this->Flash->success(__('Projeto cadastrado.'));
                return $this->redirect(['controller' => 'Members','action' => 'add', $project->id]);
            }

            $this->Flash->error(__('Unable to add the project.'));

        }

        $this->set('project', $project);
        $this->set('areas', $this->Projects->Areas->find('all'));
        $this->set('campus', $this->Projects->Campus->find('all'));
        $this->set('intellectual_properties', $this->Projects->IntellectualProperties->find('all'));
        $this->set('members_status', $this->MembersStatus->find('all'));
    }

    public function edit($id = NULL)
    {
        $this->loadModel('Members');
        $this->loadModel('MembersStatus');
        $this->set('members', $this->Members->find('all')->contain(['MembersStatus'])->where(['Members.project_id' => $id]));
        $this->set('areas', $this->Projects->Areas->find('all'));
        $this->set('campus', $this->Projects->Campus->find('all'));
        $this->set('intellectual_properties', $this->Projects->IntellectualProperties->find('all'));


        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $project = $this->Projects->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Projects->patchEntity($project, $this->request->data);
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('Your project has been updated.'));
                return $this->redirect(['action' => 'view', $project->id]);
            }
            $this->Flash->error(__('Unable to update your project.'));
        }

        $this->set('project', $project);
    }

    public function delete($id)
    {
        $project = $this->Projects->get($id);
        if ($this->Projects->delete($project)) {
            $this->Flash->success(__('The project with id: {0} has been deleted.', h($id)));

            //deletar todas as buscas
            $this->loadModel('Searches');
            $searches = $this->Searches->find('all')->where(['project_id'=>$id]);

            foreach($searches as $search){
                $this->Searches->delete($search);
            }

            return $this->redirect(['action' => 'index']);
        }
    }


}
?>