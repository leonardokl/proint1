<?php

	namespace App\Controller;

	class PostsController extends AppController
	{
        public function index() {
                $this->set('posts', $this->paginate($this->Posts));
		}

        public function  view($id = null) {
            $post = $this->Posts->get($id);
            $this->set('post', $post);
        }
	}

?>