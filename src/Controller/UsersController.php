<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 28/03/2015
 * Time: 23:58
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('signin', 'logout','contact');
    }

    public function contact()
    {

    }

    public function dashboard($data = null)
    {
        $this->loadModel('Projects');
        if ($data) {    
            $this->set( 'projects', $projects = $this->Projects->find( 'all' )->contain(['Areas', 'Status', 'Campus', 'IntellectualProperties'])->where(['YEAR(projects.created)' => $data]));
        } else {
            $this->set( 'projects', $projects = $this->Projects->find( 'all' )->contain(['Areas', 'Status', 'Campus', 'IntellectualProperties']));
        }
        $this->set('data', $data);
        //$this->set( 'projects', $projects = $this->paginate( $this->Projects->find( 'all' )->contain(['Areas', 'Status']) ) );
        //$this->set('_serialize', ['projects']);
    }

    public function dashboard2()
    {
        $this->loadModel('Projects');
        $this->set( 'projects', $projects = $this->Projects->find( 'all' )->contain(['Areas', 'Status']) );
        //$this->set('_serialize', ['projects']);
    }

    public function usersList($busca = NULL)
    {
        $busca = $this->request->data["busca"];
        if ($this->Auth->user('role') == 'user') {
            return $this->redirect('/projects/');
        }
        if ($this->request->is('post')) {
            $this->set( 'users', $users = $this->Users->find( 'all' )->where(['name LIKE' => '%'. $busca .'%']) );
        } else {
            $this->set('users', $users = $this->Users);
        }
        $this->set('_serialize', ['users']);

    }

    public function signin()
    {
        /*if ($this->Auth->user('id') != null) {
            return $this->redirect('/projects/');
        }*/

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuario cadastrado com sucesso!'));

                //Criando lista de notificações
                $this->loadModel('NotificationsLists');
                $notifications_list = $this->NotificationsLists->newEntity();
                $notifications_list->user_id = $user->id;
                $notifications_list->notifi_count = 0;
                if($this->NotificationsLists->save($notifications_list))1==1;

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function login()
    {
        if ($this->Auth->user('id') != null) {
            return $this->redirect('/projects/');
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect('/projects/');
                //return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Senha ou email incorreto!'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function index($busca = NULL)
    {
        $busca = $this->request->data["busca"];
        if ($this->Auth->user('role') == 'user') {
            return $this->redirect('/projects/');
        }
        if ($this->request->is('post')) {
            $this->set( 'users', $users = $this->paginate( $this->Users->find( 'all' )->where(['name LIKE' => '%'. $busca .'%']) ) );
        } else {
            $this->set('users', $users = $this->paginate($this->Users));
        }
    }


    public function view($id)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->loadModel('Searches');
        $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Users.id' => $id])->order(['Searches.id' => 'DESC'])));
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {

                //ADICIONANDO NOTIFICAÇÃO DE BOAS VINDAS
                $this->loadModel('Notifications');
                $notification = $this->Notifications->newEntity();;
                $notification->text = "Bem vindo!";
                $notification->user_id = $user->id;
                if($this->Notifications->save($notification))1==1;

                $this->Flash->success(__('Usuario cadastrado com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possivel adicionar o usuário.'));
        }
        $this->set('user', $user);
    }

    public function profile($id = NULL)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->Users->get($id);
        if ($this->Auth->user('id') != $user->id) {
            return $this->redirect('/projects/');
        }

        if ($this->request->is(['post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário atualizado.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possivel atualizar os dados.'));
        }

        $this->set('user', $user);
    }

    public function edit($id = NULL)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->Users->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário atualizado.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possivel atualizar o usuário.'));
        }

        $this->set('user', $user);
    }

    public function delete($id)
    {
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Usuário de id: {0} deletado.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }



}
?>