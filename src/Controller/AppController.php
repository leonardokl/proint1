<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{
    //...

    public function initialize()
    {
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Projects',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'home'
            ],
            'authenticate' => [// logar com o email
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ]
        ]);


    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['home','contact', 'Users'=>'dashboard2']);
        $this->Security->unlockedActions = array('dashboard2.json', 'index');
        $not_number = 0;
        $this->loadModel('Notifications');
        $notifications = $this->Notifications->find('all')->where(['user_id' => $this->Auth->user('id')])->andWhere(['seen' => 0]);
        $this->set('new_notifications', $notifications);

        foreach($notifications as $notification){
            $not_number++;
        }

        $this->set('not_number', $not_number);


    }


}
?>
