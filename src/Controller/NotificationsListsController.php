<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 09/04/2015
 * Time: 15:01
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


class NotificationsController extends AppController
{
    public function index(){
        $this->set('notifications_lists', $this->NotificationsLists->find('all')->where(['user_id' => $this->Auth->user('id')]));
    }

    public function empty_noti_count($id){

        $notifications_list = $this->NotificationsLists->get()->where(['user_id'=>3]);
        $notifications_list->notifi_count = 0;
        if ($this->NotificationsLists->save($notifications_list))echo "sem notificações";
    }
}
