<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 28/03/2015
 * Time: 23:58
 */
namespace App\Controller;
use Cake\Network\Email\Email;
use App\Controller\AppController;
use App\Model\Table\AreasTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class SearchesController extends AppController
{
    public function index($id = 1, $busca = NULL)
    {   
        //AUTH
        if ($this->Auth->user('role') == 'user') {
            return $this->redirect('/searches/mySearches/');
        }

        if ($this->request->is('post')) {
            $busca = $this->request->data["busca"];
            $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Searches.status_id'=>$id, 'Projects.title LIKE' => '%'. $busca .'%'])));
        } else {
            if($id != NULL){
                $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Searches.status_id'=>$id])->order(['Searches.id' => 'DESC'])));
            } else {
                $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->order(['Searches.id' => 'DESC'])));
            }
        }
       
        $this->set('searches_status', $id);
    }

    public function mySearches($id = 1, $busca = NULL)
    {
        if ($this->request->is('post')) {
            $busca = $this->request->data["busca"];
            $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Users.id' => $this->Auth->user('id'), 'Searches.status_id'=>$id, 'Projects.title LIKE' => '%'. $busca .'%'])->order(['Searches.id' => 'DESC'])));
        } else {
            if ($id != NULL){
                $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Users.id' => $this->Auth->user('id'), 'Searches.status_id'=>$id])->order(['Searches.id' => 'DESC'])));
            } else {
                $this->set('searches', $this->paginate($this->Searches->find('all')->contain(['Projects', 'Users', 'Status'])->where(['Users.id' => $this->Auth->user('id')])->order(['Searches.id' => 'DESC'])));
            }
        }   
        $this->set('searches_status', $id);
       // $this->set( 'searches', $this->paginate( $this->Searches->find( 'all' )->contain( ['Projects', 'Users', 'Status', 'Areas'] )->where(['Users.id'=>$this->Auth->user('id')]) ) );
    }

    public function add($project_id)
    {
        //Verifica se o usuário já possui a busca
        $query = $this->Searches->find( 'all' )->where(['user_id'=>$this->Auth->user('id')])->andWhere(['project_id'=>$project_id]);
        $query->select(['count' => $query->func()->count('*')]);

        $searche = $this->Searches->newEntity();
        $searche->user_id = $this->Auth->user('id');
        $searche->project_id = $project_id;

        //Salva a busca caso ela não exista
        if($query->count() < 1) {
            if ($this->Searches->save($searche)) {
                $this->Flash->success(__('Busca iniciada com sucesso!'));

                //altera status do projeto
                $this->loadModel('Projects');
                $project = $this->Projects->get($project_id);
                $project->status_id = 1;
                $this->Projects->save($project);

                return $this->redirect(['action' => 'edit/' . $searche->id]);
                //return $this->redirect(['action' => 'mySearches']);
            }
        }

        $this->Flash->error(__('Você já está buscando esse projeto'));
        return $this->redirect(['controller' => 'Projects', 'action' => 'index']);
    }

    public function edit($id = NULL, $notification = NULL)
    {
        $this->loadModel('Status');
        $this->set('status', $this->Status->find('all'));

        $this->loadModel('Descriptions');
        $this->set('descriptions', $this->Descriptions->find('all')->where(['search_id' => $id]));


        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $search = $this->Searches->get($id);

        //GAMBIARRA
        $this->loadModel('Projects');
        $this->set('project', $this->Projects->get($search->project_id));

        if ($this->request->is(['post', 'put'])) {
            $this->Searches->patchEntity($search, $this->request->data);

            //GAMBIARRA
            $this->loadModel('Projects');
            $project = $this->Projects->get($search->project_id);
            $project->status_id = $search->status_id;
            if ($this->Projects->save($project)) 1 == 1;

            if ($this->Searches->save($search)) {
                $this->Flash->success(__('Status da busca alterado.'));
            }


            return $this->redirect(['action' => 'edit', $search->id]);
            $this->Flash->error(__('Ocorreu um erro, tente novamente.'));
        }

        $this->set('search', $search);

        if($notification != NULL) {
            //NOTIFICATION VIEW
            $this->loadModel('Notifications');
            $notification = $this->Notifications->get($notification);

            if ($notification->seen == 0) {
                $notification->seen = 1;
                if ($this->Notifications->save($notification)) {
                    1 == 1;
                }
            }
        }
    }

    public function delete($id)
    {
        $search = $this->Searches->get($id);
        if ($this->Searches->delete($search)) {
            $this->loadModel('Projects');
            $project = $this->Projects->get($search->project_id);
            $project->status_id = 4;
            if ($this->Projects->save($project)) 1 == 1;
            $this->Flash->success(__('Busca de id: {0} deletada.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function listToDesignate($id = NULL, $busca = NULL)
    {
        $this->loadModel('Users');
        $busca = $this->request->data["busca"];
        
        if($this->request->is('post')) {
            $this->set( 'users', $users = $this->paginate( $this->Users->find( 'all' )->where(['name LIKE' => '%'. $busca .'%']) ) );
        } else {
            $this->set('users', $this->paginate($this->Users));
        }   
        $this->loadModel('Projects');
        $this->set('project', $this->Projects->get($id));
    }

    //ajeitar depois! função super redundante
    public function designate($project_id, $user_id)
    {
        //Verifica se o usuário já possui a busca
        $query = $this->Searches->find( 'all' )->where(['user_id'=>$user_id])->andWhere(['project_id'=>$project_id]);
        $query->select(['count' => $query->func()->count('*')]);

        $searche = $this->Searches->newEntity();
        $searche->user_id = $user_id;
        $searche->project_id = $project_id;

        //Salva a busca caso ela não exista
        if($query->count() < 1) {
            if ($this->Searches->save($searche)) {
                $this->Flash->success(__('Busca encaminhada com sucesso!'));

                //altera status do projeto
                $this->loadModel('Projects');
                $project = $this->Projects->get($project_id);
                $project->status_id = 1;
                $this->Projects->save($project);

                //ENVIO DE EMAIL
                /*$email = new Email('default');
                $email->from(['proint20151@gmail.com' => 'Cake'])
                    ->to('leonardokl@hotmail.com')
                    ->subject('About')
                    ->send('My message');*/

                //ADICIONANDO NOTIFICAÇÃO
                $this->loadModel('Notifications');
                $notification = $this->Notifications->newEntity();;
                $notification->text = "Busca designada";//$project_id
                $notification->body = "Foi solicitada uma busca de anterioridade";
                $notification->user_id = $user_id;
                $notification->search_id = $searche->id;
                if($this->Notifications->save($notification))1==1;
                return $this->redirect(['controller' => 'Projects', 'action' => 'index']);
            }
        }

        $this->Flash->error(__('Esse usuário já está buscando esse projeto'));
        return $this->redirect(['controller' => 'Projects', 'action' => 'index']);
    }


}
?>