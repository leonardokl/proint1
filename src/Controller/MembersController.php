<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 29/05/2015
 * Time: 22:41
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class MembersController extends AppController
{
    public function add($project_id = NULL, $page = NULL)
    {
        $this->loadModel('MembersStatus');
        $this->loadModel('Campus');

        $member = $this->Members->newEntity();
        if ($this->request->is('post')) {
            $member = $this->Members->patchEntity($member, $this->request->data);
            $member->project_id = $project_id;
            if ($this->Members->save($member)) {

                $this->Flash->success(__('Membro cadastrado!'));
                if($page == 'edit'){
                    return $this->redirect(['controller' =>'Projects','action' => 'edit',$member->project_id]);
                }
                return $this->redirect(['controller' =>'Projects','action' => 'index']);
            }
            $this->Flash->error(__('Ocorreu um erro'));
        }
        $this->set('member', $member);
        $this->set('members_status', $this->MembersStatus->find('all'));
        $this->set('campus', $this->Campus->find('all'));
    }

    public function edit($id = NULL)
    {
        $this->loadModel('MembersStatus');
        $this->loadModel('Campus');
        $this->set('members_status', $this->MembersStatus->find('all'));
        $this->set('campus', $this->Campus->find('all'));

        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $member = $this->Members->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Members->patchEntity($member, $this->request->data);
            if ($this->Members->save($member)) {
                $this->Flash->success(__('Membro editado'));
                return $this->redirect(['controller' => 'Projects', 'action' => 'edit', $member->project_id]);
            }
            $this->Flash->error(__('Unable to update your member.'));
        }

        $this->set('member', $member);
    }

    public function delete($id)
    {
        $member = $this->Members->get($id);
        if ($this->Members->delete($member)) {
            $this->Flash->success(__('Membro (' . $member->name . ') deletado.'));
        }

        return $this->redirect(['controller' => 'Projects', 'action' => 'edit',$member->project_id]);

    }





}
?>