<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class ExcellController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('signin', 'logout','contact');
    }

    public function projects($data = null)
    {
        $this->loadModel('Projects');

        if ($data) {    
            $this->set( 'projects', $projects = $this->Projects->find( 'all' )->contain(['Areas', 'Status', 'Campus', 'IntellectualProperties'])->where(['YEAR(projects.created)' => $data]));
        } else {
            $this->set( 'projects', $projects = $this->Projects->find( 'all' )->contain(['Areas', 'Status', 'Campus', 'IntellectualProperties']));
        }
        
        $this->set('data', $data);
    }
}
?>