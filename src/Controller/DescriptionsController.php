<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 25/05/2015
 * Time: 16:48
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DescriptionsController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index()
    {

    }

    public function add($search_id = NULL)
    {
        $description = $this->Descriptions->newEntity();
        if ($this->request->is('post')) {
            $description = $this->Descriptions->patchEntity($description, $this->request->data);
            $description->search_id = $search_id;
            if ($this->Descriptions->save($description)) {

                $this->Flash->success(__('Descrição adicionada!'));

                return $this->redirect(['controller' => 'Searches', 'action' => 'edit', $search_id]);
            }
            $this->Flash->error(__('Unable to add the description.'));
        }
        $this->set('description', $description);
    }

    public function delete($id)
    {
        $description = $this->Descriptions->get($id);
        if ($this->Descriptions->delete($description)) {
            $this->Flash->success(__('Descrição de id "{0}", removida.', h($id)));
            return $this->redirect(['controller'=>'Searches','action' => 'index']);
        }
    }

}
