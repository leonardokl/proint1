<div class="container">
    <h1><?= $post->title ?></h1>
    <p><?= $post->body ?><p>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Html->link('Back', ['controller'=>'Posts', 'action'=>'index']) ?>
        </ul>
    </div>
</div>
