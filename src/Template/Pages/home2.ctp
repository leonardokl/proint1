<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Pesquisa e Inovação
    </title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('bootstrap-theme.css') ?>
    <?= $this->Html->css('doc.css') ?>
</head>
<body class="home">
    <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a href="../" class="navbar-brand">Pesquisa e Inovação</a>-->
                <?= $this->Html->link('Pesquisa e Inovação', array('controller'=>'Pages', 'action'=>'home'), array('class'=>'navbar-brand')) ?>
            </div>
            <nav class="collapse navbar-collapse bs-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><?= $this->Html->link('Entrar', ['controller'=>'Users', 'action'=>'login']) ?></li>
                    <li><?= $this->Html->link('Cadastro', ['controller'=>'Users', 'action'=>'signin']) ?></li>
                    <li><?= $this->Html->link('Contato', ['controller'=>'Users', 'action'=>'contact']) ?></li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="jumbotron">
        <div class="container">

            <img class="img" src="img/ifal-branco.png" width="140" height="140">

            <!--<span class="glyphicon glyphicon-check" aria-hidden="true" style="font-size: 50px;"></span>-->
            <h1 cover-heading>Pesquisa e Inovação</h1>
            <p class="lead">Plataforma para controle de buscas de anterioridade</p>
            <p class="lead">
                <?= $this->Html->link('Entrar', array('controller'=>'Users', 'action'=>'login'), array('class'=>'btn btn-outline-inverse btn-lg')) ?>
            </p>
        </div>

    </div>

    <div class="container">

        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="col-lg-4" style="padding-bottom:30px">
                <img class="img-circle" src="img/2.png" width="140" height="140">
                <h2>Pesquisas de Anterioridade</h2>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4" style="padding-bottom:30px">
                <img class="img-circle" src="img/1.png" width="140" height="140">
                <h2>Propriedade Intelectual</h2>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4" style="padding-bottom:30px">
                <img class="img-circle" src="img/3.png" width="140" height="140">
                <h2>Controle de projetos</h2>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div>

    <hr class="featurette-divider">

    <footer>
        <div class="container"><p>Projeto Integrador I - 2015.1</p></div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <?= $this->Html->script('bootstrap.js'); ?>
</body>
</html>
