<?php $userL = $this->Session->read('Auth.User');?>
<div class="container">
    <ol class="breadcrumb" style="background-color:white">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li class="active">Projetos</li>
    </ol>

    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
        <div class="list-group">
            <?= $this->Html->link('Novo Projeto', ['controller'=>'Projects', 'action'=>'add'], array('class'=>'list-group-item')) ?>
        </div>
    </div>

    <!-- SEARCH INPUT -->
    <div class="input-group">
        <form action="" method="POST">
            <div class="col-lg-6">
                <div class="input-group">
                    <input name="busca" type="text" class="form-control" placeholder="Buscar projeto...">

                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><?= $this->Html->link('Todos os projetos', ['controller'=>'Projects', 'action'=>'index']) ?></li>
                            <li><?= $this->Html->link('Buscas não inicializadas', ['controller'=>'Projects', 'action'=>'index/4']) ?></li>
                            <li><?= $this->Html->link('Buscas em andamento', ['controller'=>'Projects', 'action'=>'index/1']) ?></li>
                            <li><?= $this->Html->link('Buscas encontradas', ['controller'=>'Projects', 'action'=>'index/2']) ?></li>
                            <li><?= $this->Html->link('Buscas não encontradas', ['controller'=>'Projects', 'action'=>'index/3']) ?></li>
                        </ul>
                        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>

                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </form>
    </div><!-- /SEARCH -->

    <!--<div class="col-xs-12 col-sm-9">-->

    <div class="posts index">
        <div class="list-group" style="margin-top:50px">
            <div class="list-group-item">
                <h3>Azurite</h3>
            </div>
            <div class="list-group-item">
                <h3>Azurite</h3>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><?= $this->Paginator->sort('name', 'Nome') ?></th>
                <th><?= $this->Paginator->sort('area_id', 'Área do conhecimento') ?></th>
                <th><?= $this->Paginator->sort('status_id', 'Status') ?></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $results = 0;
            foreach($projects as $project): $results++?>
                <tr>
                    <td><?= $this->Html->link($project->title, ['controller'=>'Projects', 'action'=>'view', $project->id]) ?></td>
                    <td><?= $project->area->name?></td>
                    <td><?= $project->status->name ?></td>
                    <?php if ($userL['role'] == 'admin') {
                        if ($project->status_id == 4) {
                         echo "<td title='Encaminhar'>" . $this->Html->link('', ['controller' => 'Searches', 'action' => 'listToDesignate', $project->id], array('class' => 'glyphicon glyphicon-arrow-right')) . "</td>";
                        } else
                            echo "<td></td>";
                    }?>
                    <?php if($project->status_id == 4) {
                        echo "<td title='Começar busca'>" . $this->Html->link('', ['controller' => 'Searches', 'action' => 'add', $project->id], array('class' => 'glyphicon glyphicon-search')) . "</td>";
                    } else {
                        echo "<td></td>";
                    }?>

                    <?php if ($userL['role'] == 'admin') {
                    echo "<td title='Editar'>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'edit', $project->id], array('class'=>'glyphicon glyphicon-pencil')) . "</td>";
                    }?>
                    <?php if ($userL['role'] == 'admin' || $project->user_id == $userL['id']) {
                        echo "<td title='Deletar'>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'delete', $project->id], array('class'=>'glyphicon glyphicon-trash')) . "</td>";
                    }?>

                </tr>
            <?php endforeach; if($results == 0)echo "<tr><td><h2>Nenhum projeto encontrado!</h2></td></tr>";?>
            </tbody>
        </table>
    </div>

    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Previous');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Next >');
            ?>
        </ul>
    </div>
    <!--</div>-->

</div>