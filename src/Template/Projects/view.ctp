<?php $userL = $this->Session->read('Auth.User');?>
<div class="container">

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Detalhes</li>
    </ol>
    
    
    <?php foreach($projects as $project):?>
      <span class="pull-right" style="color:#777777">Cadastrado em <?php echo $project->created = date("d/m/Y");?></span>
    <!-- BUTTONS -->
    <ul class="rounded-buttons" style="clear:both">
        <?php 
            if ($userL['role'] == 'admin' || $userL['id'] == $project->user_id) :
        ?>
            <li><?=$this->Html->link('', ['controller'=>'Projects', 'action'=>'edit', $project->id], array('class'=>'glyphicon glyphicon-pencil', 'title' => 'Editar projeto'))?></li>                 
        <?php 
            endif 
        ?>
        <?php
            if($busca != NULL) {
                if ($userL['role'] == 'admin' || $userL['id'] == $busca->user_id) {
                    echo "<li>" . $this->Html->link('', ['controller' => 'Searches', 'action' => 'edit', $busca->id], array('class' => 'glyphicon glyphicon-search', 'title' => 'Editar busca')) . "</li>";                                  
                }
            }
        ?> 
    </ul>

    <!-- MAIN -->
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:10px">
            <?php
                $cor = ['', 'info', 'danger', 'success', 'warning'];
            ?>
            <span class="label label-<?=$cor[$project->status->id]?> pull-right"><?=$project->status->name?></span>
            <h2><?= $project->title ?></h2>
            <p><em><?= $project->area->name?></em></p><hr>
            <div class="col-md-9" style="overflow:hidden;float:none">
            <div class="col-sm-4" style="text-allign:left">
                <span class="glyphicon glyphicon-map-marker"></span>Campus
                <p><h4><?=$project->campus->name?></h4></p>
            </div>
            <div class="col-sm-4" style="text-allign:left">
                <span class="glyphicon glyphicon-barcode"></span> Propriedade intelectual
                <p><h4><?=$project->intellectual_property->name?></h4></p>
            </div>
            </div>

            <!-- AVALIADOR -->
            <?php 
                if($busca != NULL):?> <hr style="clear:both">
                Avaliador:
                    <blockquote ><em>         
                        <p><?=$busca->user->name?> <!--<a href="mailto:$busca->user->email?Subject=Sistema de Anterioridade" target="_top" title="Enviar e-mail" class="glyphicon glyphicon-envelope"></a>--></p></em></blockquote> 
            <?php 
                endif 
            ?>  
                                            
        </div>
    </div><!-- /MAIN -->

    <?php endforeach ?>

    <!-- RESUMO -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-edit"></a>
        </li>
        <li>
            <h3>Resumo</h3>
        </li>
    </ul>

    <div class="list-group" style="margin-top:30px;">
        <div class="list-group-item" style="padding-bottom:30px">
            <p align="justify"><?= nl2br($project->abstract) ?></p>
        </div>
    </div>

    <!-- MEMBERS -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-user"></a>
        </li>
        <li>
            <h3>Membros</h3>
        </li>
    </ul>
    
    <div class="list-group" style="margin-top:30px">    
            <?php
                    $member_all = "";
                    $i = 0;
                    foreach($members as $member){                        
                        $i++;
                        echo "<div class='list-group-item' style='padding-bottom:50px'><h3>$member->name</h3></td><td><em>". $member->members_status->status ."<em><ul class='rounded-buttons pull-right' style='clear:both'><li><a href='mailto:$member->email?Subject=Sistema de Anterioridade' target='_top' title='Enviar e-mail' class='glyphicon glyphicon-envelope', 'title' => 'Enviar email'></a></li></div>";
                    } 
                    if($i == 0) {
                        echo "<div class='list-group-item' style='padding-bottom:50px'><h3>Nenhum membro cadastrado</h3></div>";
                    }
                    ?>     
        </div>
    </div>

</div>

<script>
    document.getElementById("projetos").className = "ativado";
</script>
