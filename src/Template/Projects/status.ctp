<?php $userL = $this->Session->read('Auth.User');?>
<div class="container">
    <h1>Projetos</h1>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" style="text-allign:center">
        <div class="list-group">
            <?= $this->Html->link('Novo Projeto', ['controller'=>'Projects', 'action'=>'add'], array('class'=>'list-group-item')) ?>
            <?= $this->Html->link('Projetos sem buscas', ['controller'=>'Projects', 'action'=>'noSearch'], array('class'=>'list-group-item')) ?>
            <?= $this->Html->link('Buscas nãao inicializadas', ['controller'=>'Projects', 'action'=>'status/1'], array('class'=>'list-group-item')) ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9">

        <?php //$this->Html->link('Novo projeto', ['controller'=>'Projects', 'action'=>'add'], array('class'=>'glyphicon glyphicon-plus')) ?>
        <div class="posts index">
            <table class="table">
                <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('area_id') ?></th>
                    <th><?= $this->Paginator->sort('Status') ?></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($projects as $project): ?>
                    <tr>
                        <td><?= $project->id ?></td>
                        <td><?= $this->Html->link($project->title, ['controller'=>'Projects', 'action'=>'view', $project->id]) ?></td>
                        <td><?= $project->area->name?></td>
                        <td><?= $project->status->name ?></td>
                        <?php if ($userL['role'] == 'admin') {
                            echo "<td title='Encaminhar'>" . $this->Html->link('', ['controller'=>'Searches', 'action'=>'listToDesignate', $project->id], array('class'=>'glyphicon glyphicon-arrow-right')) . "</td>";
                        }?>
                        <td title="Começar busca"><?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'add', $project->id], array('class'=>'glyphicon glyphicon-search')) ?></td>
                        <?php if ($userL['role'] == 'admin') {
                            echo "<td title='Editar'>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'edit', $project->id], array('class'=>'glyphicon glyphicon-pencil')) . "</td>";
                        }?>
                        <?php if ($userL['role'] == 'admin' || $project->user_id == $userL['id']) {
                            echo "<td title='Deletar'>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'delete', $project->id], array('class'=>'glyphicon glyphicon-trash')) . "</td>";
                        }?>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="paginator">
            <ul class="pagination">
                <?php
                echo $this->Paginator->prev('< Previous');
                echo $this->Paginator->numbers();
                echo $this->Paginator->next('Next >');
                ?>
            </ul>
        </div>
    </div>
</div>