<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Cadastrar Projeto</li>
    </ol>

    <!-- CADASTRAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-plus"></a>
        </li>
        <li>
            <h3>Novo projeto</h3>
        </li>
    </ul>

    <div class="form-signin">
        <?= $this->Form->create($project) ?>        
        <div class="list-group" style="margin-top:30px">
            <div class="list-group-item" style="padding-bottom:10px">
                <label for="title">Título</label><br>
                <?= $this->Form->input(('title'),  array('class'=>'form-control', 'label'=>false)) ?><br>
                <label for="abstract">Resumo</label><br>
                <?= $this->Form->input(('abstract'),  array('class'=>'form-control', 'label'=>false)) ?><br>
                <label for="area_id">Área do conhecimento</label><br>
                <select name="area_id">
                <?php
                    foreach($areas as $area){
                        echo "<option value=$area->id>$area->name</option>";
                    }
                ?>
                </select><br><br>
                <label for="campus_id">Campus</label><br>
                <select name="campus_id">
                    <?php
                    foreach($campus as $camp){
                        echo "<option value=$camp->id>$camp->name</option>";
                    }
                    ?>
                </select><br><br>
                <label for="intellectual_property_id">Propriedade intelectual</label><br>
                <select name="intellectual_property_id">
                    <?php
                    foreach($intellectual_properties as $intellectual_property){
                        echo "<option value=$intellectual_property->id>$intellectual_property->name</option>";
                    }
                    ?>
                </select><br><br>
                <hr>
                <?= $this->Form->button(__('Cadastrar'), array('class'=>'btn btn-success', 'id' => 'confirm')); ?>
        </div>
        </div>
        
        <?= $this->Form->end() ?><?= $this->Flash->render() ?>
    </div>
</div>

<script>
    document.getElementById("projetos").className = "ativado";
</script>