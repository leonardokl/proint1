<?php $userL = $this->Session->read('Auth.User');?>

<script>
    document.getElementById("projetos").className = "ativado";
</script>

<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li class="active">Projetos</li>
    </ol>

    <!-- ADD PROJECT BTN -->
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
        <div class="list-group">
            <?= $this->Html->link('Novo Projeto', ['controller'=>'Projects', 'action'=>'add'], array('class'=>'list-group-item ')) ?>
        </div>
    </div>

    <!-- SEARCH INPUT -->
    <div class="input-group">
        <form action="" method="POST">
            <div class="col-lg-6">
                <div class="input-group">
                    <input name="busca" type="text" class="form-control" placeholder="Buscar projeto...">

                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><?= $this->Html->link('Todos os projetos', ['controller'=>'Projects', 'action'=>'index']) ?></li>
                            <li><?= $this->Html->link('Buscas não inicializadas', ['controller'=>'Projects', 'action'=>'index/4']) ?></li>
                            <li><?= $this->Html->link('Buscas em andamento', ['controller'=>'Projects', 'action'=>'index/1']) ?></li>
                            <li><?= $this->Html->link('Buscas encontradas', ['controller'=>'Projects', 'action'=>'index/2']) ?></li>
                            <li><?= $this->Html->link('Buscas não encontradas', ['controller'=>'Projects', 'action'=>'index/3']) ?></li>
                        </ul>
                        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>

                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </form>
    </div><!-- /SEARCH -->

    <div class="posts index">
        <div class="list-group" style="margin-top:50px">
            <?php
                $results = 0;
            foreach($projects as $project): $results++?>
            <div class="list-group-item" style="padding-bottom:50px">
                <?php
                    $cor = ['', 'info', 'danger', 'success', 'warning'];
                ?>
                <span class="label label-<?=$cor[$project->status->id]?> pull-right"><?= $project->status->name ?></span>
                <h3><?= $this->Html->link($project->title, ['controller'=>'Projects', 'action'=>'view', $project->id]) ?></h3>

                    <p><em><?= $project->area->name?></em></p>
                    
                    <!-- BUTTONS -->
                    <ul id="index" class="rounded-buttons-index pull-right" style="clear:both">
                    <?php if ($userL['role'] == 'admin') {
                        if ($project->status_id == 4) {
                         echo "<li>" . $this->Html->link('', ['controller' => 'Searches', 'action' => 'listToDesignate', $project->id], array('class' => 'glyphicon glyphicon-arrow-right', 'title' => 'Encaminhar projeto')) . "</li>";
                        } 
                    }?>
                    <?php if($project->status_id == 4) {
                        echo "<li>" . $this->Html->link('', ['controller' => 'Searches', 'action' => 'add', $project->id], array('class' => 'glyphicon glyphicon-search', 'title' => 'Fazer busca de anterioridade')) . "</li>";
                    } ?>

                    <?php if ($userL['role'] == 'admin' || $project->user_id == $userL['id']) {
                    echo "<li>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'edit', $project->id], array('class'=>'glyphicon glyphicon-pencil', 'title' => 'Editar projeto')) . "</li>";
                    }?>
                    <?php if ($userL['role'] == 'admin' || $project->user_id == $userL['id']) {
                        echo "<li>" . $this->Html->link('', ['controller'=>'Projects', 'action'=>'delete', $project->id], array('class'=>'glyphicon glyphicon-trash', 'title' => 'Deletar projeto')) . "</li>";
                    }?>                    
                    </ul>
                
            </div>
            <?php endforeach; if($results == 0)echo "<h4>Nenhum projeto encontrado!</h4>";?>            
        </div>                    
            
    </div>

    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Anterior');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Proxíma >');
            ?>
        </ul>
    </div>
    <!--</div>-->

</div>

