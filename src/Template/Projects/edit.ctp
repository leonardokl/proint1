<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Editar</li>
    </ol>

    <!-- EDITAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-pencil"></a>
        </li>
        <li>
            <h3>Editar</h3>
        </li>
    </ul>

    <?= $this->Form->create($project);?>
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:10px">         
            <label for="title">Título</label><br>             
            <?= $this->Form->input(('title'),  array('class'=>'form-control', 'label'=>false));?><br>
            <label for="abstract">Resumo</label><br>             
            <?= $this->Form->input(('abstract'),  array('class'=>'form-control', 'label'=>false)) ?><br>        
            <label for="title">Área do conhecimento</label><br>
            <select name="area_id">
            <?php
                foreach($areas as $area){
                    if($area->id == $project->area_id) {
                        echo "<option selected value=$area->id>$area->name</option>";
                    } else {
                        echo "<option value=$area->id>$area->name</option>";
                    }
                }
            ?>
            </select><br><br>
            
            <label for="campus_id">Campus</label><br>
            <select name="campus_id">
                <?php
                    foreach($campus as $camp){
                        if($camp->id == $project->campus_id) {
                            echo "<option selected value=$camp->id>$camp->name</option>";
                        } else {
                            echo "<option value=$camp->id>$camp->name</option>";
                        }
                    }
                ?>
            </select><br><br>
                          
            <label for="intellectual_property_id">Propriedade intelectual</label><br>
            <select name="intellectual_property_id">
                <?php
                    foreach($intellectual_properties as $intellectual_property){
                        if($intellectual_property->id == $project->intellectual_property_id) {
                            echo "<option selected value=$intellectual_property->id>$intellectual_property->name</option>";
                        } else {
                            echo "<option value=$intellectual_property->id>$intellectual_property->name</option>";
                        }
                    }
                ?>
            </select><br><br>
            <?= $this->Form->button(__('Salvar'), array('class'=>'btn btn-success', 'id' => 'confirm'));?>
        </div>
    </div>
    <?= $this->Form->end();?>

    <!-- MEMBERS TITLE -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-user"></a>
        </li>
        <li>
            <h3>Membros</h3>
        </li>
    </ul>

    <!-- MEMBERS -->
    <ul class="rounded-buttons" style="clear:both">
            <li><?= $this->Html->link('', ['controller'=>'Members', 'action'=>'add', $project->id, 'edit'], array('class'=>'glyphicon glyphicon-plus', 'title' => 'Novo membro')) ?></li>
        </ul>
    <div class="list-group" style="margin-top:30px">   
            <?php 
                $i = 0;
                foreach($members as $member){
                    $i++;
                    
                    echo "<div class='list-group-item' style='padding-bottom:50px'>";
                    echo "<h3>" . $member->name . " </h3><em>". $member->members_status->status ."</em>";
                    echo "<ul class='rounded-buttons pull-right' style='clear:both'>";
                    echo "<li style='padding-right:5px'>";
                    echo "<a href='mailto:" . $member->email . "?Subject=Sistema de Anterioridade' target='_top' title='Enviar e-mail' class='glyphicon glyphicon-envelope'></a>";
                    echo "</li>";
                    echo "<li style='padding-right:5px'>";
                    echo $this->Html->link('', ['controller'=>'Members', 'action'=>'edit', $member->id], array('class'=>'glyphicon glyphicon-pencil', 'title' => 'Editar membro')) . "</li>";
                    echo "<li>";
                    echo $this->Html->link('', ['controller'=>'Members', 'action'=>'delete', $member->id], array('class'=>'glyphicon glyphicon-trash', 'title' => 'Deletar membro'));
                    echo "</li>";
                    echo "</ul>";
                    echo "</div>";
                }
                if($i == 0) {
                    echo "<div class='list-group-item' style='padding-bottom:50px'>";
                    echo "<h3>Nenhum membro cadastrado</h3>";
                    echo "</div>";
                }
            ?>    
    </div>
    </div>             
    </div>

    

</div>

<script>
    document.getElementById("projetos").className = "ativado";
</script>