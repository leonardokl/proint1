<!DOCTYPE html>
<html pt_br>
<head>
    <?= $this->Html->script('angular.min.js'); ?>
    <?php $userL = $this->Session->read('Auth.User');?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        IFAL:
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('icon', $this->Html->url('/favicon.png')); ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('bootstrap-theme.css') ?>
    <?= $this->Html->css('proint.css') ?>
    <?= $this->Html->css('flash.css') ?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css') ?>



    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<header class="navbar navbar-inverse navbar-top" style="margin-bottom: 0">
    <div class="container">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- BOTÃO DE MENU RESPONSIVO -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- NAV ESQUERDO -->
                <ul id="logo" style="margin:0" class="nav navbar-nav navbar-left">
                    <li><?= $this->Html->link('Pesquisa e Inovação', array('controller'=>'Pages', 'action'=>'home'), array('class'=>'navbar-brand')) ?></li>
                </ul>
                <!--<a class="navbar-brand" href="#" style="padding-bottom:10px;">
                    <img width="35px" alt="Brand" src="/cake/img/ifal-branco.png" >
                </a>-->

            </div>

            <!-- NAV DIREITO -->
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <!-- LINKS DO MENUS -->
                    <li id="projetos"><?= $this->Html->link('Projetos', ['controller'=>'Projects', 'action'=>'index']) ?></li>
                    
                    <!-- ANTERIORIDADES -->
                    <?php 
                        if($userL['role'] == 'admin') {
                    ?>
                    <li id="anterioridades-admin"class="dropdown" title="Anterioridades">
                        
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Anterioridade<span class='caret'></span></a>                                                                                                                           
                        <ul class="dropdown-menu" role="menu">                            
                            <li>
                                <?= $this->Html->link('Minhas Buscas', ['controller'=>'Searches', 'action'=>'mySearches']) ?>
                            </li>  
                            
                                    <?= "<li>" . $this->Html->link('Todas as Buscas', ['controller' => 'Searches', 'action' => 'index']) . "<li>"?>                                                       
                        </ul>
                    </li>
                    <?php
                        echo "<li id='usuarios'>" . $this->Html->link('Usuários', ['controller'=>'Users', 'action'=>'index']) . "</li>";
                        echo "<li id='relatorios'>" . $this->Html->link('Relatorios', ['controller'=>'Users', 'action'=>'dashboard']) . "</li>";
                        } else {
                            echo "<li id='anterioridades-admin'>".$this->Html->link('Anterioridades', ['controller'=>'Searches', 'action'=>'mySearches'])."</li>";
                        }
                    ?>                 
                    <!-- /ANTERIORIDADES -->                                      

                    <!-- NOTIFICAÇÕES -->
                    <li id="notificacoes" class="dropdown" title="Notificações">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-envelope"></span>
                            <?php
                            if($not_number > 0) {
                                echo "<span style = 'background-color:#E54E4E' class='badge' >$not_number</span >";
                            }
                            ?>
                            <span class='caret' ></span >
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                                foreach($new_notifications as $notification){
                                    if($notification->search_id != NULL){
                                        echo "<li>" . $this->Html->link($notification->text, ['controller' => 'Searches', 'action' => 'edit', $notification->search_id, $notification->id]) . "</li>";

                                    } else {
                                        echo "<li>" . $this->Html->link($notification->text, ['controller' => 'Notifications', 'action' => 'view', $notification->id], array('style' => 'color:#28b463')) . "</li>";
                                    }
                                }
                            ?>
                            <li role="presentation" class="divider"></li>
                            <li><?= $this->Html->link('Notificações', ['controller'=>'Notifications', 'action'=>'index']) ?></li>
                        </ul>
                    </li><!-- /NOTIFICAÇÕES -->

                    <!-- USUÁRIO -->
                    <li id="perfil" class="dropdown" title="Usuário">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation"><a href="#"><?php
                                    if(!empty($userL)) ?>
                                    <?= $userL['email']?></a>
                            </li>
                            <li><?= $this->Html->link('Perfil', ['controller'=>'Users', 'action'=>'profile', $userL['id']]) ?></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><?= $this->Html->link('Sair', ['controller'=>'Users', 'action'=>'logout']) ?></li>
                        </ul>
                    </li><!-- /USUÁRIO -->

                </ul>
            </div>
        </div>
    </div><!-- /container-->
</header>

<div id="container">
    <div id="content">
        <?= $this->Flash->render() ?>

        <div class="row">
            <?= $this->fetch('content') ?>
        </div>
    </div>

    <footer>
    </footer>

    <?= $this->Html->script('jquery-2.1.3.min.js'); ?>
    <?= $this->Html->script('ajax.js'); ?>
    <?= $this->Html->script('bootstrap.min.js'); ?>
    
</div>
</body>
</html>
