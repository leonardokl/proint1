<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Login
    </title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">

    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('doc.css') ?>
    <?= $this->Html->css('login.css') ?>
    <?= $this->Html->css('flash.css') ?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a href="../" class="navbar-brand">Pesquisa e Inovação</a>-->
            <?= $this->Html->link('Pesquisa e Inovação', array('controller'=>'Pages', 'action'=>'home'), array('class'=>'navbar-brand')) ?>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><?= $this->Html->link('Entrar', ['controller'=>'Users', 'action'=>'login']) ?></li>
                <li><?= $this->Html->link('Cadastro', ['controller'=>'Users', 'action'=>'signin']) ?></li>
                <li><?= $this->Html->link('Contato', ['controller'=>'Users', 'action'=>'contact']) ?></li>
            </ul>
        </nav>
    </div>
</header>
<div id="container">
    <?= $this->Flash->render('auth') ?>
    <div id="content">
        <?= $this->Flash->render() ?>

        <div class="row">
            <?= $this->fetch('content') ?>
        </div>
    </div>

    <footer>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <?= $this->Html->script('bootstrap.js'); ?>
</div>
</body>
</html>

