<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb" >
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Designar busca</li>
    </ol>
 
    <!-- MAIN -->
    <div class="posts index">
        <h2>Escolha um avaliador para o projeto</h2>

        <!-- SEARCH INPUT -->
        <div id="search" class="input-group"style="margin-bottom:30px; margin-top:30px">
            <form action="" method="POST">          
                    <div class="input-group"><input type="hidden" id="id" value="<?=$project->id?>"></input>
                        <input name="busca" type="text" class="form-control" placeholder="Buscar usuário..."></input>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                    </div><!-- /input-group --> 
            </form>
        </div>

        <div class="list-group" style="margin-top:30px;clear:both">
            <?php 
                foreach($users as $user): 
            ?>
            <div class="list-group-item" style="padding-bottom:50px">
                <h3><?= $this->Html->link($user->name, ['controller'=>'Users', 'action'=>'view', $user->id]) ?></h3>
                <p><?= $user->email ?></p>

                <ul class="rounded-buttons pull-right" style="clear:both">
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'designate', $project->id, $user->id], array('class'=>'glyphicon glyphicon-arrow-right')) ?>
                    </li>
                </ul>
            </div>
            <?php 
                endforeach; 
            ?>
        </div>           
    </div><!-- /MAIN -->

    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Previous');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Next >');
            ?>
        </ul>
    </div>
</div>

<script>
    document.getElementById("projetos").className = "ativado";
</script>