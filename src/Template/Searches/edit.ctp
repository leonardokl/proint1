<div class="container">

    <!-- BREADCRUMB-->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Buscas', array('controller'=>'Searches', 'action'=>'index')) ?></li>
        <li class="active">Editar</li>
    </ol>
    
    <?= $this->Form->create($search);?>
    <!-- titulo do projeto -->
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:30px">            
            <h2><?= $this->Html->link($project->title, array('controller'=>'Projects', 'action'=>'view', $project->id)) ?></h2>
            <hr>
            <h2><?=$project->area->name?></h2>

            <select name="status_id" onchange='this.form.submit()'>
            <?php
                foreach($status as $result) {
                    if($result->id != 4){
                        if($result->id == $search->status_id) {
                            echo "<h3><option selected value=$result->id>$result->name</option></h3>";
                        } else {
                            echo "<h3><option value=$result->id>$result->name</option></h3>";
                        }
                    }
                }
            ?>
            </select>
        </div>
    </div>

    
    
    
    <span style="visibility:hidden"><?= $this->Form->button(__('Salvar'));?></span>
    <?= $this->Form->end();?>

    <!-- DESCRIPTIONS -->    
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-tags"></a>
        </li>
        <li>
            <h3>Descrições</h3>
        </li>
    </ul>

    <!-- ADD DESCRIPTION BTN-->
    <ul class="rounded-buttons" style="clear:both">
        <li><?= $this->Html->link('', ['controller'=>'Descriptions', 'action'=>'add', $search->id], array('class'=>'glyphicon glyphicon-plus', 'title' => 'Nova descrição')) ?></li>
    </ul>
    
    <div class="list-group" style="margin-top:30px">        
        <?php
        $result = 0;
            foreach($descriptions as $description){
                $result++;
                echo "<div class='list-group-item' style='padding-bottom:50px'><span class='pull-right'>" . $this->Html->link('', ['controller'=>'Descriptions', 'action'=>'delete', $description->id], array('class'=>'glyphicon glyphicon-remove')) . "</span>";
                echo "<h3>" . $description->text . "</h3></div>";
            }

            if($result == 0) {
                echo "<div class='list-group-item' style='padding-bottom:50px'>";
                echo "<h3>Nenhuma descrição adicionada</h3>";
                echo "</div>";
            }
        ?>
        </div>
    </div>

</div>

<script>
    document.getElementById("anterioridades-admin").className = "ativado";
</script>