<div class="container">
    
    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li class="active">Anterioridades</li>
    </ol>

    <!-- TABS -->
    <ul id="tab" class="nav nav-pills" style="margin:auto">
        
        <li  role="presentation" <?php if($searches_status == 1) echo "class='active blue'"?>><?=$this->Html->link('Em andamento', ['controller'=>'Searches', 'action'=>'mySearches'])?></li>
        <li  title="Não encontrado" role="presentation" <?php if($searches_status == 2) echo "class='active red'"?>><?=$this->Html->link('', ['controller'=>'Searches', 'action'=>'mySearches', 2], array('class'=>'glyphicon glyphicon-remove'))?></li>
        <li  title="Patentes" role="presentation" <?php if($searches_status == 3) echo "class='active green'"?>><?=$this->Html->link('', ['controller'=>'Searches', 'action'=>'mySearches', 3], array('class'=>'glyphicon glyphicon-star'))?></li>          
    </ul>
 
    <!-- MAIN -->
    <div class="posts index">
        <!-- SEARCH INPUT -->   
        <div id="search" class="input-group"> 
            <form action="" method="POST">   
                <div class="input-group" style="margin-top:30px;">
                    <input name="busca" type="text" class="form-control" placeholder="Buscar usuário..."></input>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                </div><!-- /input-group -->   
            </form>
        </div>

        <div class="list-group" style="margin-top:30px;clear:both">
            <?php $i = 0; foreach($searches as $search): $i++;?>
            <div class="list-group-item" style="padding-bottom:50px">
                <?php
                    $cor = ['', 'info', 'danger', 'success', 'warning'];
                ?>
                <span class="label label-<?=$cor[$search->status->id]?> pull-right"><?= $search->status->name ?></span>
                <h3><?= $this->Html->link($search->project->title, array('controller'=>'Projects', 'action'=>'view', $search->project->id)) ?></h3>

                <ul class="rounded-buttons-index pull-right" style="clear:both">
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'edit', $search->id], array('class'=>'glyphicon glyphicon-pencil', 'title' => 'Editar busca')) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'delete', $search->id], array('class'=>'glyphicon glyphicon-trash', 'title' => 'Deletar busca')) ?>
                    </li>                    
                </ul>
            </div>
            <?php 
                endforeach;

                if( $i == 0 ){
                    echo "<div class='list-group-item' style='padding-bottom:50px'><h3>Nenhuma busca encontrada!</h3></div>";
                }
            ?>
        </div>                           
    </div><!-- index end -->

    <!-- PAGINATOR -->
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Previous');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Next >');
            ?>
        </ul>
    </div>
</div>

<script>
    document.getElementById("anterioridades-admin").className = "ativado";
</script>