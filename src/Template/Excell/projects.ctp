<?php 
	$this->layout = false;

	header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
	header( "Content-disposition: attachment; filename=Pesquisa e Inovação - Projetos" . $data . ".xls" );

	echo 'Id' . "\t" . "Projetos" . "\t" . mb_convert_encoding('Áreas do Conhecimento', 'UTF-16LE', 'UTF-8') . "\t" . 'Campus' . "\t" . 'Propriedades Intelectuais' . "\n";
	
	foreach($projects as $project) {
		echo $project->id . "\t";
		echo mb_convert_encoding($project->title, 'UTF-16LE', 'UTF-8') . "\t";
		echo mb_convert_encoding($project->area->name, 'UTF-16LE', 'UTF-8') . "\t";
		echo mb_convert_encoding($project->campus->name, 'UTF-16LE', 'UTF-8') . "\t";
		echo mb_convert_encoding($project->intellectual_property->name, 'UTF-16LE', 'UTF-8') . "\n";	
	} 


?>

