<div class="container">
    <h1>Intellectual Properties</h1>

    <div class="posts index">
        <table class="table">
            <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>

            </tr>
            </thead>
            <tbody>
            <?php foreach($intellectual_properties as $area): ?>
                <tr>
                    <td><?= $area->id ?></td>
                    <td><?= $area->name ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Previous');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Next >');
            ?>
        </ul>
    </div>
</div>