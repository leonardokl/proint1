<?= $this->Flash->render() ?>
<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Cadastrar Membro</li>
    </ol>

    <!-- EDITAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-user"></a>
        </li>
        <li>
            <h3>Novo membro</h3>
        </li>
    </ul>

    <!-- MAIN -->
    <div class="list-group" style="margin-top:30px">       
        <div class="list-group-item" style="padding-bottom:50px">
            <div class="form-signin">
                <?= $this->Form->create($member) ?>                
                <label for="name">Nome</label><br>
                <?= $this->Form->input(('name'),  array('class'=>'form-control', 'label'=>false)) ?><br>
                <label for="email">Email</label><br>
                <?= $this->Form->input(('email'),  array('class'=>'form-control', 'label'=>false)) ?><br>
                <label for="members_status_id">Função</label><br>
                <select name="members_status_id">
                    <?php
                    foreach($members_status as $member_status){
                        echo "<option value=$member_status->id>$member_status->status</option>";
                    }
                    ?>
                </select><br><br>
                <label for="campus_id">Campus</label><br>
                <select name="campus_id">
                    <?php
                    foreach($campus as $camp){
                        echo "<option value=$camp->id>$camp->name</option>";
                    }
                    ?>
                </select><br><br>
                <?= $this->Form->button(__('Incluir membro'), array('class'=>'btn btn-success')); ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= /*$this->Form->input(('area_id'),  [
    'options' => ['1' => 'CIÊNCIAS EXATAS E DA TERRA', '2' => 'CIÊNCIAS BIOLÓGICAS']
],  array('class'=>'form-control')) */1?>