
<div class="container">
    <ol class="breadcrumb" >
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Projetos', array('controller'=>'Projects', 'action'=>'index')) ?></li>
        <li class="active">Editar membro</li>
    </ol>

    <!-- EDITAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-pencil"></a>
        </li>
        <li>
            <h3>Editar membro</h3>
        </li>
    </ul>

    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:50px">
            <?= $this->Form->create($member);?>
            <label for="name">Nome</label><br>
            <!--<input type="text" name="title">-->
            <?= $this->Form->input(('name'),  array('class'=>'form-control', 'label'=>false));?><br>
            <label for="email">Email</label><br>
            <?= $this->Form->input(('email'),  array('class'=>'form-control', 'label'=>false)) ?><br>
            <label for="members_status_id">Função</label><br>
            <select name="members_status_id">
                <?php
                foreach($members_status as $member_status){
                    if($member->members_status_id == $member_status->id) {
                        echo "<option selected value=$member_status->id>$member_status->status</option>";
                    } else {
                        echo "<option value=$member_status->id>$member_status->status</option>";
                    }
                }
                ?>
            </select><br><br>

            <label for="campus_id">Campus</label><br>
            <select name="campus_id">
                <?php
                foreach($campus as $camp){
                    if($camp->id == $project->campus_id) {
                        echo "<option selected value=$camp->id>$camp->name</option>";
                    } else {
                        echo "<option value=$camp->id>$camp->name</option>";
                    }
                }
                ?>
            </select><br><br><hr>
            <?= $this->Form->button(__('Salvar'), array('class'=>'btn btn-success'));?>
        </div>
    </div>

    
    <?= $this->Form->end();?>

</div>