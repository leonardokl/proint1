<?php
$this->layout = 'notLogged';
?>

<script>
    document.getElementById("entrar").className = "ativado";
</script>

<style>
    body {
        background-color: #28b463;
    }
</style>

<div class="form-signin">	
    <?= $this->Form->create() ?>
    <h1><?= __('Login') ?></h1>
    <?= $this->Form->input(('email'),  array('class'=>'form-control')  )?>
    <label for="password">Senha</label><br>
    <?= $this->Form->input(('password'), array('class'=>'form-control', 'label'=>false) )?>
    <?= $this->Form->button(__('Entrar'), array('class'=>'btn btn-outline-inverse btn-lg')); ?>
    <?= $this->Form->end() ?>
    <br><b><a style="color:white" href="signin">Cadastrar</a></b>
    <?= $this->Flash->render() ?>
</div>