<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Usuarios', array('controller'=>'Users', 'action'=>'index')) ?></li>
        <li class="active">Detalhes</li>
    </ol>

    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-user"></a>
        </li>
        <li>
            <h3>Usuário</h3>
        </li>
    </ul>
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:50px">
            <span class="label label-success pull-right"><?= $user->role ?></span>
            <h2><?= $user->name ?></h2>
            <p><?= $user->email ?><p>                        
        </div>
    </div>

    <ul class="rounded-buttons-title" style="clear:both;margin-top:50px">
        <li>
            <a class="glyphicon glyphicon-search"></a>
        </li>
        <li>
            <h3>Buscas</h3>
        </li>
    </ul>

    <div class="list-group" style="margin-top:30px">
        <?php 
            $cor = ['', 'info', 'danger', 'success', 'warning'];

            foreach($searches as $search){
                $results++;        
                echo "<div class='list-group-item' style='padding-bottom:50px'>";
                echo "<span class='label label-" . $cor[$search->status->id] .  " pull-right'>" . $search->status->name . "</span>";
                echo "<h3>" . $this->Html->link($search->project->title, array('controller'=>'Projects', 'action'=>'view', $search->project->id)) . "</h3>";
            ?>
                <ul class="rounded-buttons pull-right" style="clear:both">                    
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'edit', $search->id], array('class'=>'glyphicon glyphicon-pencil', 'title' => 'Editar busca')) ?>
                    </li> 
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Searches', 'action'=>'delete', $search->id], array('class'=>'glyphicon glyphicon-trash', 'title' => 'Deletar busca')) ?>
                    </li>                    
                </ul>
            <?php     
                if( $results == 0 ) {
                    echo "<h3>Nenhuma busca sendo feita por esse usuário</h3>";
                }
                echo "</div>";
            }
        ?>      
    </div>

    <!-- PAGINATOR -->
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Anterior');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Proxíma >');
            ?>
        </ul>
    </div>
</div>

<script>
    document.getElementById("usuarios").className = "ativado";
</script>