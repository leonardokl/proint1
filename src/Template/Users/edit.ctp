<?php $userL = $this->Session->read('Auth.User');?>
<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Usuários', array('controller'=>'Users', 'action'=>'index')) ?></li>
        <li class="active">Editar</li>
    </ol>

    <!-- EDITAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-pencil"></a>
        </li>
        <li>
            <h3>Editar</h3>
        </li>
    </ul>

    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:30px">
            <?= $this->Form->create($user);?>
            <?= $this->Form->input(('name'),  array('class'=>'form-control'));?>
            <?= $this->Form->input(('email'),  array('class'=>'form-control'));?>

            <?php if($userL['role'] == 'admin'): ?>
            <label for="role">Role</label><br>
            <select name="role">
                <option <?php if($user->role == 'user') echo "selected"?> value="user" >user</option>";
                <option <?php if($user->role == 'admin') echo "selected"?> value="admin">admin</option>"
            </select><br><br>
            <?php endif ?><hr>
            <?= $this->Form->button(__('Salvar'), array('class'=>'btn btn-success'));?>
            <?= $this->Form->end();?>

        </div>
    </div>

</div>

<script>
    document.getElementById("usuarios").className = "ativado";
</script>