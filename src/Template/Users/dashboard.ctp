<script>
    document.getElementById("relatorios").className = "ativado";
</script>

<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Usuários', array('controller'=>'Users', 'action'=>'index')) ?></li>
        <li class="active">Relatórios</li>
    </ol>
    <?= $this->Html->script('jquery-2.1.3.min.js'); ?>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>                

        <!-- RELATÓRIOS -->
        <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
            <li>
                <a class="glyphicon glyphicon-object-align-bottom"></a>
            </li>
            <li>
                <h3>Relatórios</h3>
            </li>
        </ul>
    
        
        
        <div class="dropdown pull-right">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <?php
                if($data) {
                    echo $data;
                } else {
                    echo "Todas as datas";
                }
            ?>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><?= $this->Html->link('Todas as datas', ['controller'=>'Users', 'action'=>'dashboard']) ?></li>
            <li><?= $this->Html->link('2015', ['controller'=>'Users', 'action'=>'dashboard', 2015]) ?></li>
            <li><?= $this->Html->link('2014', ['controller'=>'Users', 'action'=>'dashboard', 2014]) ?></li>  
            <li><?= $this->Html->link('2013', ['controller'=>'Users', 'action'=>'dashboard', 2013]) ?></li>          
          </ul>
        </div>                

        <!-- EXCELL DOWNLOAD -->
        <ul class="rounded-buttons">
            <li>
                <?=$this->Html->link('', ['controller'=>'Excell', 'action'=>'projects', $data], array('class'=>'glyphicon glyphicon-download-alt', 'title' => 'Baixar planilha'))?>
            </li>
        </ul>

        <!-- CHARTS -->
        <div class="list-group" style="margin-top:30px"> 
            <div class="list-group-item" style="padding-bottom:50px">
                <div id="chart" style="min-width: 310px; max-width: 800px; height: 400px; width: 100%x;margin: 0 auto"></div>
            </div>
        </div>

        <div class="list-group" style="margin-top:30px"> 
            <div class="list-group-item" style="padding-bottom:50px">
                <div id="chart_status" style="min-width: 310px; max-width: 800px; height: 400px; width: 100%x;margin: 0 auto"></div>
            </div>
        </div>

        <div class="list-group" style="margin-top:30px"> 
            <div class="list-group-item" style="padding-bottom:50px">
                <div id="chart_campus" style="min-width: 310px; max-width: 800px; height: 400px; width: 100%x;margin: 0 auto"></div>
            </div>
        </div>

        <div class="list-group" style="margin-top:30px"> 
            <div class="list-group-item" style="padding-bottom:50px">
                <div id="chart_propriedade" style="min-width: 310px; max-width: 800px; height: 400px; width: 100%x;margin: 0 auto"></div>
            </div>
        </div>

        </div>

        
        </div>
    </div>
    
    <!--É o poder da gambiarra-->
    <?php
        $projects_count = 0;

        $cet = 0;
        $cb = 0;
        $eng = 0;
        $mul = 0;
        $ca = 0;
        $cs = 0;
        $ch = 0;
        $csa = 0;
        $lla = 0;

        $nao_iniciado = 0;
        $andamento = 0;
        $patente = 0;
        $encontrado = 0;

        $maceio = 0;
        $marechal = 0;
        $arapiraca = 0;
        $coruripe = 0;
        $murici = 0;
        $palmeira = 0;
        $penedo = 0;
        $piranhas = 0;
        $santana = 0;
        $sao_miguel = 0;
        $satuba = 0;
        $maragogi = 0;

        $pi_desenho = 0;
        $pi_indicacao = 0;
        $pi_patente = 0;
        $pi_programa = 0;
        $pi_direitos = 0;

        foreach($projects as $project){
            $projects_count++;
            switch($project->area_id){
                case 1: $cet++;
                    break;
                case 2: $cb++;
                    break;
                case 3: $eng++;
                    break;
                case 4: $mul++;
                    break;
                case 5: $ca++;
                    break;
                case 6: $cs++;
                    break;
                case 7: $ch++;
                    break;
                case 8: $csa++;
                    break;
                case 9: $lla++;
                    break;
            }

            switch($project->status_id){
                case 1: $andamento++;
                    break;
                case 2: $encontrado++;
                    break;
                case 3: $patente++;
                    break;
                case 4: $nao_iniciado++;
                    break;
            }   

            switch($project->campus_id) {
                case 1: $maceio++;
                    break;
                case 2: $marechal++;
                    break;
                case 3: $arapiraca++;
                    break;
                case 4: $coruripe++;
                    break;
                case 5: $murici++;
                    break;
                case 6: $palmeira++;
                    break;
                case 7: $penedo++;
                    break;
                case 8: $piranhas++;
                    break;
                case 9: $santana++;
                    break;
                case 10: $sao_miguel++;
                    break;
                case 11: $satuba++;
                    break;
                case 12: $maragogi++;
                    break;

            }    

            switch($project->intellectual_property_id) {
                case 1: $pi_desenho++;
                    break;
                case 2: $pi_indicacao++;
                    break;
                case 3: $pi_patente++;
                    break;
                case 4: $pi_programa++;
                    break;
                case 5: $pi_direitos++;
                    break;
            }        
        }
        
    ?>
    <input type="hidden" id="projects_count" value="<?= $projects_count ?>">

    <input type="hidden" id="cet" value="<?= $cet ?>">
    <input type="hidden" id="cb" value="<?= $cb ?>">
    <input type="hidden" id="eng" value="<?= $eng ?>">
    <input type="hidden" id="mul" value="<?= $mul ?>">
    <input type="hidden" id="ca" value="<?= $ca ?>">
    <input type="hidden" id="cs" value="<?= $cs ?>">
    <input type="hidden" id="ch" value="<?= $ch ?>">
    <input type="hidden" id="csa" value="<?= $csa ?>">
    <input type="hidden" id="lla" value="<?= $lla ?>">

    <input type="hidden" id="andamento" value="<?= $andamento ?>">
    <input type="hidden" id="encontrado" value="<?= $encontrado ?>">
    <input type="hidden" id="patente" value="<?= $patente ?>">
    <input type="hidden" id="nao_iniciado" value="<?= $nao_iniciado ?>">

    <!-- Campus -->
    <input type="hidden" id="maceio" value="<?= $maceio ?>">
    <input type="hidden" id="marechal" value="<?= $marechal ?>">
    <input type="hidden" id="arapiraca" value="<?= $arapiraca ?>">
    <input type="hidden" id="coruripe" value="<?= $coruripe ?>">
    <input type="hidden" id="murici" value="<?= $murici ?>">
    <input type="hidden" id="palmeira" value="<?= $palmeira ?>">
    <input type="hidden" id="penedo" value="<?= $penedo ?>">
    <input type="hidden" id="piranhas" value="<?= $piranhas ?>">
    <input type="hidden" id="santana" value="<?= $santana ?>">
    <input type="hidden" id="sao_miguel" value="<?= $sao_miguel ?>">
    <input type="hidden" id="satuba" value="<?= $satuba ?>">
    <input type="hidden" id="maragogi" value="<?= $maragogi ?>">

    <!-- Intellectual -->
    <input type="hidden" id="pi_desenho" value="<?= $pi_desenho ?>">
    <input type="hidden" id="pi_indicacao" value="<?= $pi_indicacao ?>">
    <input type="hidden" id="pi_patente" value="<?= $pi_patente ?>">
    <input type="hidden" id="pi_programa" value="<?= $pi_programa ?>">
    <input type="hidden" id="pi_direitos" value="<?= $pi_direitos ?>">
    
    
    <script>
        $(function () {
            var cet = $('#cet').val();
            var cb = $('#cb').val();
            var eng = $('#eng').val();
            var mul = $('#mul').val();
            var ca = $('#ca').val();
            var cs = $('#cs').val();
            var ch = $('#ch').val();
            var csa = $('#csa').val();
            var lla = $('#lla').val();

            $('#chart').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Projetos'
                },
                subtitle: {
                    text: 'Áreas do Conhecimento'
                },
                xAxis: {
                    categories: ['CIÊNCIAS EXATAS E DA TERRA', 'CIÊNCIAS BIOLÓGICAS', 'ENGENHARIAS', 'CIÊNCIAS AGRÁRIAS', 'CIÊNCIAS DA SAÚDE', 'CIÊNCIAS HUMANAS', 'CIÊNCIAS SOCIAIS E APLICADAS', 'LINGUISTICA, LETRAS E ARTES', 'MULTIDISCIPLINAR'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Unidade',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ''
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 100,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Total: ' + parseInt($('#projects_count').val()),
                    data: [parseInt(cet), parseInt(cb), parseInt(eng), parseInt(ca), parseInt(cs), parseInt(ch), parseInt(csa), parseInt(lla), parseInt(mul)]
                }       ]
            });
        });

        //STATUS CHART
        $(document).ready(function () {
            var nao_iniciado = parseInt($('#nao_iniciado').val());
            var andamento = parseInt($('#andamento').val());
            var patente = parseInt($('#patente').val());
            var encontrado = parseInt($('#encontrado').val());
            // Build the chart
            $('#chart_status').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'Status'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Projetos',
                    data: [
                        ['Não iniciado',   nao_iniciado],
                        ['Busca em andamento',       andamento],
                        {
                            name: 'Patente',
                            y: patente,
                            sliced: true,
                            selected: true
                        },
                        ['Busca encontrada',    encontrado]
                    ]
                }]
            });
        });
        
            var maceio = parseInt($('#maceio').val());
            var marechal = parseInt($('#marechal').val());
            var arapiraca = parseInt($('#arapiraca').val());
            var coruripe = parseInt($('#coruripe').val());
            var murici = parseInt($('#murici').val());
            var palmeira = parseInt($('#palmeira').val());
            var penedo = parseInt($('#penedo').val());
            var piranhas = parseInt($('#piranhas').val());
            var santana = parseInt($('#santana').val());
            var sao_miguel = parseInt($('#sao_miguel').val());
            var satuba = parseInt($('#satuba').val());
            var maragogi = parseInt($('#maragogi').val());

        $('#chart_campus').highcharts({

            

            chart: {
                type: 'bar'
            },
            title: {
                text: 'Campus'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['Arapiraca', 'Coruripe', 'Maceió', 'Maragogi', 'Marechal', 'Murici', 'Palmeiras', 'Penedo', 'Piranhas', 'Santana', 'Sao Miguel', 'Satuba'],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Unidade',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            
            credits: {
                enabled: false
            },
            series: [{
                name: 'Projetos',
                data: [arapiraca, coruripe, maceio, maragogi, marechal, murici, palmeira, penedo, piranhas, santana, sao_miguel, satuba]
            }]
        });
        
            var pi_desenho = parseInt($('#pi_desenho').val());
            var pi_indicacao = parseInt($('#pi_indicacao').val());
            var pi_patente = parseInt($('#pi_patente').val());
            var pi_programa = parseInt($('#pi_programa').val());
            var pi_direitos = parseInt($('#pi_direitos').val());    
        $('#chart_propriedade').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Propriedade Intelectual'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Unidade'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Projetos'
            },
            series: [{
                name: 'Population',
                data: [
                    ['Desenho Industrial', pi_desenho],
                    ['Indicação Geográfica', pi_indicacao],
                    ['Patente', pi_patente],
                    ['Programa de Computador', pi_programa],
                    ['Direitos Autorais', pi_direitos]
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
        
    </script>
</div>

