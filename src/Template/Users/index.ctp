<div class="container">
    
    
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li class="active">Usuários</li>
    </ol>

    <!-- ADD USER BUTTON -->
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" style="text-allign:center">
        <div class="list-group">
            <?= $this->Html->link('Novo Usuário', ['controller'=>'Users', 'action'=>'add'], array('class'=>'list-group-item')) ?>
        </div>
    </div>

    <!-- SEARCH INPUT -->
    <div class="input-group">
        <form action="users/index/" method="POST">
            <div class="col-lg-6">
                <div class="input-group">
                    <input name="busca" type="text" class="form-control" placeholder="Buscar usuário..."></input>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </form>
    </div>

    <!-- MAIN -->
    <div class="posts index">
        <div class="list-group" style="margin-top:50px">
            <?php
                $results = 0;
                foreach($users as $user): $results++;
            ?>
            <div class="list-group-item" style="padding-bottom:50px">
                <span class="label label-<?php if($user->role == 'admin')echo 'info';else echo 'success';?> pull-right"><?= $user->role ?></span>
                <h3><?= $this->Html->link($user->name, ['controller'=>'Users', 'action'=>'view', $user->id]) ?></h3>
                <p><em><?= $user->email ?></em></p>

                <ul class="rounded-buttons-index pull-right" style="clear:both">
                    <li><?= $this->Html->link('', ['controller'=>'Users', 'action'=>'edit', $user->id], array('class'=>'glyphicon glyphicon-pencil')) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('', ['controller'=>'Users', 'action'=>'delete', $user->id], array('class'=>'glyphicon glyphicon-trash')) ?>
                    </li>
                </ul>
            </div>
            <?php 
                endforeach;
                if($results == 0) {
                    echo "<tr><td><h2>Nenhum Usuário foi encontrado!</h2></td></tr>";
            }
        ?> 
        </div>
                                                              
    </div><!-- /MAIN -->

    <!-- PAGINATOR -->
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('< Previous');
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('Next >');
            ?>
        </ul>
    </div>
</div>

<script>
    document.getElementById("usuarios").className = "ativado";
</script>