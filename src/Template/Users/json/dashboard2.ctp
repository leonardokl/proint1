<?php
foreach ($projects as $project) {
    unset($project->generated_html);
}
echo json_encode(compact('projects'), JSON_PRETTY_PRINT);
?>