<?php
$this->layout = 'notLogged';
?>

<script>
    document.getElementById("cadastro").className = "ativado";
</script>

<style>
    body {
        background-color: #28b463;
    }
</style>

<div class="form-signin">
    <?= $this->Form->create($user) ?>
    <h1><?= __('Cadastrar') ?></h1>
    <?= $this->Form->input(('email'),  array('class'=>'form-control')) ?>
    <label for="password">Senha</label><br>
    <?= $this->Form->input(('password'),  array('class'=>'form-control', 'label'=>false))  ?>
    <label for="name">Nome</label><br>
    <?= $this->Form->input(('name'),  array('class'=>'form-control', 'label'=>false)) ?><br>
    <?= $this->Form->button(__('Cadastrar'), array('class'=>'btn btn-outline-inverse btn-lg')); ?>
    <?= $this->Form->end() ?><?= $this->Flash->render() ?>
</div>
