<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Usuários', array('controller'=>'Users', 'action'=>'index')) ?></li>
        <li class="active">Perfil</li>
    </ol>

    <!-- EDITAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-user"></a>
        </li>
        <li>
            <h3>Perfil</h3>
        </li>
    </ul>
    <div class="list-group" style="margin-top:30px">     
        <div class="list-group-item" style="padding-bottom:30px">
            <?= $this->Form->create($user);?>
            <label for="name">Nome</label><br>
            <?= $this->Form->input(('name'),  array('class'=>'form-control', 'label'=>false));?>
            <?= $this->Form->input(('email'),  array('class'=>'form-control'));?><br><hr>
            <?= $this->Form->button(__('Salvar'), array('class'=>'btn btn-success', 'id' => 'confirm'));?>
            <?= $this->Form->end();?>
        </div>
    </div>

</div>

<script>
    document.getElementById("perfil").className = "ativado";
</script>