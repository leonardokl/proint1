<div class="container">
    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Usuários', array('controller'=>'Users', 'action'=>'index')) ?></li>
        <li class="active">Cadastrar Usuário</li>
    </ol>

    <!-- MAIN -->
    <!-- ADD -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-plus"></a>
        </li>
        <li>
            <h3>Novo usuário</h3>
        </li>
    </ul>
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:30px">
            <div class="form-signin">
                <?= $this->Form->create($user) ?>                
                <?= $this->Form->input(('email'),  array('class'=>'form-control')) ?>
                <label for="password">Senha</label><br>
                <?= $this->Form->input(('password'),  array('class'=>'form-control', 'label'=>false))  ?>
                <label for="name">Nome</label><br>
                <?= $this->Form->input(('name'),  array('class'=>'form-control', 'label'=>false)) ?><br>
                <hr>
                <?= $this->Form->button(__('Cadastrar'), array('class'=>'btn btn-success', 'id' => 'confirm')); ?>
                <?= $this->Form->end() ?><?= $this->Flash->render() ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById("usuarios").className = "ativado";
</script>