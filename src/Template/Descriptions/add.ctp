<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Searches', array('controller'=>'Searches', 'action'=>'mySearches')) ?></li>
        <li class="active">Descrição</li>
    </ol>

    <!-- CADASTRAR -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-plus"></a>
        </li>
        <li>
            <h3>Nova descrição</h3>
        </li>
    </ul>

    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:30px"> 
            <div class="form-signin">
                <?= $this->Form->create($description) ?>                
                <label for="text">Descrição</label><br> 
                <?= $this->Form->input(('text'),  array('class'=>'form-control', 'label'=>false)) ?>
                <br><hr>
                <?= $this->Form->button(__('Cadastrar'), array('class'=>'btn btn-success')); ?>
                <?= $this->Form->end() ?><?= $this->Flash->render() ?>
            </div>
        </div>
    </div>
</div>
