<div class="container">

    <!-- BREADCRUMB -->
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li><?= $this->Html->link('Notificações', array('controller'=>'Notifications', 'action'=>'index')) ?></li>
        <li class="active"><?=$notification->id?></li>
    </ol>

    <!-- NOTIFICAÇOES -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-envelope"></a>
        </li>
        <li>
            <h3>Notificação</h3>
        </li>
    </ul>

    <!-- MAIN -->
    <div class="list-group" style="margin-top:30px">
        <div class="list-group-item" style="padding-bottom:50px;">
            <span class="pull-right"><?= $this->Html->link('', ['controller'=>'Notifications', 'action'=>'delete', $notification->id], array('class'=>'glyphicon glyphicon-remove', 'title' => 'Deletar notificação')) ?></span>
            
            <span class="pull-right" style="color:#777777; margin-right:20px"><?php echo $notification->created = date("d/m/Y");?></span>

            <h2><?= $notification->text ?></h2>
            
            <ul class="rounded-buttons pull-right" style="clear:both">
                <?php if($notification->search_id != NULL){
                    echo "<li>" . $this->Html->link('', ['controller'=>'Searches', 'action'=>'edit', $notification->search_id], array('class'=>'glyphicon glyphicon-search', 'title' => 'Editar busca')) . "</li>";
                }?>         
            </ul>                   
        </div>
    </div><!-- /MAIN -->

</div>

<script>
    document.getElementById("notificacoes").className = "ativado";
</script>