<div class="container">
    <ol class="breadcrumb">
        <li><?= $this->Html->link('Home', array('controller'=>'Pages', 'action'=>'home')) ?></li>
        <li class="active">Notificações</li>
    </ol>

    <!-- NOTIFICAÇOES -->
    <ul class="rounded-buttons-title" style="clear:both;margin-top:30px">
        <li>
            <a class="glyphicon glyphicon-envelope"></a>
        </li>
        <li>
            <h3>Notificações</h3>
        </li>
    </ul>

    <div class="posts index">
        <div class="list-group" style="margin-top:30px">
            <?php
                $results = 0;
                foreach($notifications as $notification):
                    $results++;
            ?>
            <div class="list-group-item" style="padding-bottom:20px;<?php 
                    echo $seen = ($notification->seen == 1) ? '' : "background-color:#E4E4E4;";
                ?>" >                
                <span class="pull-right"><?= $this->Html->link('', ['controller'=>'Notifications', 'action'=>'delete', $notification->id], array('class'=>'glyphicon glyphicon-remove', 'title' => 'Deletar notificação')) ?></span>
                <span class="pull-right" style="color:#777777; margin-right:20px"><?php echo $notification->created = date("d/m/Y");?></span>
                
                <h3><?= $this->Html->link($notification->text, ['controller'=>'Notifications', 'action'=>'view', $notification->id])?></h3>   
            </div>
            <?php
                endforeach;
                if($results == 0){
                    echo "<h2>Você não possui notificações!</h2>";
                }
            ?>
        </div>             
    </div>
    <!-- /INDEX -->
</div>

<script>
    document.getElementById("notificacoes").className = "ativado";
</script>