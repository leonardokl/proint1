-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16-Jul-2015 às 02:12
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `proint`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
`id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `areas`
--

INSERT INTO `areas` (`id`, `name`) VALUES
(1, 'CIÊNCIAS EXATAS E DA TERRA'),
(2, 'CIÊNCIAS BIOLÓGICAS'),
(3, 'ENGENHARIAS'),
(4, 'MULTIDISCIPLINAR'),
(5, 'CIÊNCIAS AGRÁRIAS'),
(6, 'CIÊNCIAS DA SAÚDE'),
(7, 'CIÊNCIAS HUMANAS'),
(8, 'CIÊNCIAS SOCIAIS APLICADAS'),
(9, 'LINGUÍSTICA, LETRAS E ARTES');

-- --------------------------------------------------------

--
-- Estrutura da tabela `campus`
--

CREATE TABLE IF NOT EXISTS `campus` (
`id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `campus`
--

INSERT INTO `campus` (`id`, `name`) VALUES
(1, 'Maceió'),
(2, 'Marechal Deodoro'),
(3, 'Arapiraca'),
(4, 'Coruripe'),
(5, 'Murici'),
(6, 'Palmeira dos Índios'),
(7, 'Penedo'),
(8, 'Piranhas'),
(9, 'Santana do Ipanema'),
(10, 'São Miguel dos Campos'),
(11, 'Satuba'),
(12, 'Maragogi');

-- --------------------------------------------------------

--
-- Estrutura da tabela `descriptions`
--

CREATE TABLE IF NOT EXISTS `descriptions` (
`id` int(12) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `search_id` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `descriptions`
--

INSERT INTO `descriptions` (`id`, `text`, `search_id`) VALUES
(6, 'Creme', 8),
(7, 'Não encontrei nada', 28),
(8, 'Projetos', 35),
(9, 'http://www.voltsdigital.com.br/projetos/', 35),
(10, 'Lorem ipsum', 35),
(11, 'otglkgçlhk', 41),
(12, 'ddfdf', 27);

-- --------------------------------------------------------

--
-- Estrutura da tabela `intellectual_properties`
--

CREATE TABLE IF NOT EXISTS `intellectual_properties` (
`id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `intellectual_properties`
--

INSERT INTO `intellectual_properties` (`id`, `name`) VALUES
(1, 'DESENHO INDUSTRIAL'),
(2, 'INDICAÇÃO GEOGRÁFICA'),
(3, 'PATENTE'),
(4, 'PROGRAMA DE COMPUTADOR'),
(5, 'DIREITOS AUTORAIS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `members_status_id` int(12) NOT NULL,
  `grade` varchar(50) DEFAULT NULL,
  `campus_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `members_status_id`, `grade`, `campus_id`, `project_id`) VALUES
(5, 'Roberta', 'erer@email.com', 1, NULL, 1, 9),
(6, 'JOsé', 'edriane@uol.com.br', 1, NULL, 1, 13),
(7, 'Edmária Rodrigues Araújo', 'xxxx@xxxx.com', 1, NULL, 1, 23),
(9, 'Cleversson', 'Cleversson@email.com', 2, NULL, 2, 9),
(12, 'Jessica', 'jessica@email.com', 4, NULL, 1, 9),
(13, 'Caio', 'caio@email.com', 1, NULL, 1, 24),
(14, 'Lara', 'lara@email.com', 1, NULL, 1, 25),
(15, 'ze', 'ademar.paulino@ifal.edu.br', 1, NULL, 1, 27),
(16, 'Marquez', 'marquez@email.com', 4, NULL, 2, 9),
(17, 'Bruno Calvacante', 'bruno@email.com', 1, NULL, 1, 29),
(18, 'Claudio Estêvão Bergamini', 'claudio@email.com', 2, NULL, 1, 23),
(19, 'Antônio José de Souza Luna', 'antonio@email.com', 1, NULL, 1, 22),
(20, 'Karollyne Marques de Lima', 'karl@email.com', 1, NULL, 6, 21),
(21, 'Thiago Belo Gois', 'thiago@email.com', 1, NULL, 1, 20),
(22, 'ABEL COELHO DA SILVA NETO', 'abel@email.com', 1, NULL, 1, 30),
(23, 'ABEL COELHO DA SILVA NETO', 'abel@email.com', 1, NULL, 1, 31),
(24, 'ANA PAULA SANTOS DE MELO FIORI', 'ana@email.com', 1, NULL, 2, 32),
(25, 'CICERO DE OLIVEIRA COSTA', 'cicero@email.com', 1, NULL, 11, 33),
(26, 'CICERO DE OLIVEIRA COSTA', 'cicero@email.com', 1, NULL, 11, 34),
(27, 'DANIELE GOMES DE LYRA', 'dani@email.com', 1, NULL, 11, 35),
(28, 'DANIELE GOMES DE LYRA', 'dani@email.com', 1, NULL, 11, 36),
(29, 'DJALMA ALBUQUERQUE BARROS FILHO', 'djalma@email.com', 1, NULL, 2, 37),
(30, 'MARCÍLIO FERREIRA DE SOUZA JÚNIOR', 'marcilio@email.com', 1, NULL, 1, 38),
(31, 'MARCÍLIO FERREIRA DE SOUZA JÚNIOR', 'marcilio@email.com', 1, NULL, 1, 39),
(32, 'MAURICIO VIEIRA DIAS JUNIOR', 'email@email.com', 1, NULL, 3, 40),
(33, 'MONICA XIMENES CARNEIRO CUNHA', 'monica@email.com', 1, NULL, 1, 41),
(34, 'MÔNICA XIMENES CARNEIRO CUNHA', 'monica@email.com', 1, NULL, 1, 42),
(35, 'PHABYANNO RODRIGUES LIMA', 'asda@email.com', 1, NULL, 1, 43),
(36, 'RAFAEL THYAGO ANTONELLO', 'asdsa@email.com', 1, NULL, 6, 44),
(37, 'RAFAEL THYAGO ANTONELLO', 'sds@email.com', 1, NULL, 6, 45),
(38, 'WLADIA BESSA DA CRUZ', 'wladia@email.com', 1, NULL, 1, 46),
(39, 'ADEMAR DA SILVA PAULINO', 'ademar@email.com', 1, NULL, 11, 47),
(40, 'ANDRÉ SUÊLDO TAVARES DE LIMA', 'aa@email.com', 1, NULL, 12, 48),
(41, 'ANDRÉ SUÊLDO TAVARES DE LIMA', 'email@ema.com', 1, NULL, 12, 49),
(42, 'ÂNGELA FROEHLICH', 'an@e.com', 1, NULL, 11, 50),
(43, 'ÂNGELA FROEHLICH', 'ang@email.com', 1, NULL, 11, 51),
(44, 'CLAUDIVAN COSTA DE LIMA', 'c@e.com', 1, NULL, 11, 52),
(45, 'DANIEL DE MAGALHÃES ARAUJO', 'email@rme.com', 1, NULL, 11, 53),
(46, 'DANIEL DE MAGALHÃES ARAUJO', 'daniel@email.com', 1, NULL, 11, 54),
(47, 'TEREZINHA FERREIRA XAVIER ', 'terezinha@email.com', 1, NULL, 12, 55),
(48, 'DANIELLE DOS SANTOS TAVARES PEREIRA', 'danielle@email.com', 1, NULL, 5, 56),
(49, 'QUITÉRIA VIEIRA BELO', 'q@email.com', 1, NULL, 6, 57),
(50, 'MARCOS ANDRÉ RODRIGUES DA SILVA JÚNIOR', 'marco@email.com', 1, NULL, 6, 58),
(51, 'MARCOS ANDRÉ RODRIGUES DA SILVA JÚNIOR', 'marcos@email.com', 1, NULL, 6, 59),
(52, 'MARIA DE FATIMA FEITOSA AMORIM', 'maria@email.com', 1, NULL, 2, 60),
(53, 'MARIA REGINA GONÇALVES DOS SANTOS', 'maria@email.com', 1, NULL, 7, 61),
(54, 'ARIADNE AGUIAR VITÓRIO MENDONÇA', 'ariadne@email.com', 1, NULL, 11, 62),
(55, 'CÍCERO JULIÃO DA SILVA JUNIOR ', 'ci@email.com', 1, NULL, 6, 63),
(56, 'CÍCERO JULIÃO DA SILVA JUNIOR', 'cicero@e.com', 1, NULL, 6, 64),
(57, 'NÁDIA MARA DA SILVEIRA', 'nadia@email.com', 1, NULL, 1, 65),
(58, 'VICTOR SOUZA SGARBI', 'sgarbi@emailc.om', 1, NULL, 2, 66),
(59, 'ÁUREA LUIZA QUIXABEIRA ROSA E SILVA RAPÔSO', 'aurea@email.com', 1, NULL, 1, 67),
(60, 'EDJA LAURINDO DA SILVA', 'edja@email.com', 1, NULL, 6, 68),
(61, 'ELVYS ALVES SOARES', 'elvys@email.com', 1, NULL, 6, 69),
(62, 'JOSÉ ARNÓBIO ARAÚJO JÚNIOR ', 'arnobio@mail.com', 1, NULL, 3, 70),
(63, 'MANOEL SANTOS MARTINS', 'mano@email.com', 1, NULL, 1, 71),
(64, 'MARCELO ASSIS CORREA', 's@email.com', 1, NULL, 1, 72),
(65, 'ROBERTO FERNANDES DA CONCEIÇÃO', 'roberto@email.com', 1, NULL, 6, 73),
(66, 'RODRIGO MERO SARMENTO DA SILVA', 'sa@email.com', 1, NULL, 1, 74),
(67, 'RODRIGO MERO SARMENTO DA SILVA', 'smtp@gmail.com', 1, NULL, 1, 75),
(68, 'RODRIGO MERO SARMENTO DA SILVA', 'sadnsa@email.co', 1, NULL, 1, 76),
(69, 'SHEYLA KAROLINA JUSTINO MARQUES', 'sa@email.com', 1, NULL, 6, 77),
(70, 'SHEYLA KAROLINA JUSTINO MARQUES', 'saa@email.com', 1, NULL, 1, 78);

-- --------------------------------------------------------

--
-- Estrutura da tabela `members_lists`
--

CREATE TABLE IF NOT EXISTS `members_lists` (
`id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `members_status`
--

CREATE TABLE IF NOT EXISTS `members_status` (
`id` int(12) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `members_status`
--

INSERT INTO `members_status` (`id`, `status`) VALUES
(1, 'Orientador'),
(2, 'Co-orientador'),
(3, 'Bolsista'),
(4, 'Voluntário');

-- --------------------------------------------------------

--
-- Estrutura da tabela `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
`id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `text` varchar(255) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `search_id` int(12) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `body` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `text`, `seen`, `search_id`, `title`, `created`, `body`) VALUES
(8, 1, 'Projeto 18 designado', 1, NULL, NULL, '2015-07-07 20:04:21', ''),
(9, 1, 'Projeto 17 designado', 1, NULL, NULL, '2015-07-07 20:04:21', ''),
(10, 1, 'Projeto 17 designado', 1, NULL, NULL, '2015-07-07 20:04:21', ''),
(11, 1, 'Projeto 17 designado', 1, NULL, NULL, '2015-07-07 20:04:21', ''),
(12, 9, 'Bem vindo!', 0, NULL, NULL, '2015-07-07 20:04:21', ''),
(20, 1, 'Um projeto lhe foi designado', 1, 36, NULL, '2015-07-07 20:04:21', ''),
(23, 9, 'Um projeto lhe foi designado', 0, 42, NULL, '2015-07-07 20:04:21', ''),
(24, 8, 'Um projeto lhe foi designado', 0, 43, NULL, '2015-07-07 20:04:21', ''),
(26, 1, 'Um projeto lhe foi designado', 1, 45, NULL, '2015-07-14 20:04:38', ''),
(28, 7, 'Busca designada', 1, 47, NULL, '2015-07-15 23:41:04', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `notifications_lists`
--

CREATE TABLE IF NOT EXISTS `notifications_lists` (
`id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `notifications_count` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `notifications_lists`
--

INSERT INTO `notifications_lists` (`id`, `user_id`, `notifications_count`) VALUES
(1, 1, 0),
(4, 4, 0),
(5, 5, 0),
(6, 6, 0),
(7, 7, 0),
(8, 10, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `title` varchar(255) NOT NULL,
  `abstract` text,
  `status_id` int(12) DEFAULT '4',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `area_id` int(12) NOT NULL,
  `intellectual_property_id` int(12) DEFAULT NULL,
  `campus_id` int(12) NOT NULL,
  `mermbers_list_id` int(12) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projects`
--

INSERT INTO `projects` (`id`, `user_id`, `title`, `abstract`, `status_id`, `created`, `modified`, `area_id`, `intellectual_property_id`, `campus_id`, `mermbers_list_id`, `date`) VALUES
(9, 1, 'Lórém ipsum dolor sit ámet', 'Lórém ipsum dolor sit ámet, vólúptúa sápientém maíestatis id éôs, dôlores prinçipés prí ad, nàm an fallí zril. Quó nemore possim ét, vel éu mêis fíerént, dicam tàçimàtês íntellegám has an. Tâcimàtes adólesçens delicàtissimi at ést, véri errõr áeterno àt vel, his homeró fàcéte accommodare at. Est cu nullã detracto.\r\n\r\n Offendit necessitâtibus seá ex, séà cu lãoreet môlestiáê sçrípserit. Mei óratio dolorés ei, ét meís pârtem has, ex útroqué fuissét atomõrum séd. Quem vídêrer oblique mél id, ut pri fàbúlàs cónceptam. Víde fugit choro eum án, per cu verear communê vôluptãria. Êa dõçéndí vivendum çonsequuntur eum. Nêc no quõdsi pártíendo âliquandô, íd vel solum oportere imperdiet.\r\n\r\n Vim ád dicãt gráécis démócritúm, ex eós invidunt referréntúr, ius minim ãliquid in. Duo ne áútem detràxít, cú duó ením ludus dicéret. Id séd prôbó sensibus. Àd duo atqúi cãusâe cômprehensám, ut dicànt dóming êam. Ut evértítur ãssúéverít per, vix né récusabo erroríbús pàtriôquê. Malis repudiãndae pro te.\r\n\r\n Ut noster quaeréndum has. Pósteá mollis probatús nam in, erát erânt aperiri éi êos, eúm ãn persius nominavi sadipscing. Id per legêre vivendum võlutpat, requê vólumus tôrquátos an sêd, éros sale cu eum. Et vim deseruisse interessét, dictã íntégre usú et. Ei nostro sapíéntem torquãtos cum. Atqúi pôpulo çóncludaturqúe êá mea.\r\n', 3, '2015-04-18 17:18:41', '2015-04-18 17:18:41', 1, 5, 2, NULL, NULL),
(10, 1, 'Offendit necessitâtibus seá ex', 'Ut noster quaeréndum has. Pósteá mollis probatús nam in, erát erânt aperiri éi êos, eúm ãn persius nominavi sadipscing. Id per legêre vivendum võlutpat, requê vólumus tôrquátos an sêd, éros sale cu eum. Et vim deseruisse interessét, dictã íntégre usú et. Ei nostro sapíéntem torquãtos cum. Atqúi pôpulo çóncludaturqúe êá mea.\r\n\r\n Decorê principes déterrúisset vim ad, ius no pêrtinãx concludátúrqúe, eum in luptatum perséçúti dissentiunt. Luptãtúm repúdiárê concludáturque mei ei, assúm sónêt qualisque âd çum. No muçius fábulás detrâxit qúo, pri àd legendôs tâcimàtês. Àpériám âccusàm déseruisse id nec, éi modus erípuit fúíssét has. Gràeçi impedit fastídii pri cu, meí regíónê nôstrud âdolesçens éx, vix câsé périculis ut.\r\n\r\n Volúmús oportere expéténdis ét nàm, has melius nusquám eligéndi ei. Ipsúm veritus his et, ádhuc dicànt semper víx id, ênim neglêgéntur tê ius. Nõvum tôrquatos prõ ut, illum virís témpor éa cum, ne ridêns alienum sâlútatus eâm. Eôs at error prômpta çôrrúmpit, ut sed rêquê tritání disputatiôni. Êrrém qúãéstíô eloquéntíam at vim.\r\n\r\n Sea nô quodsi partíendo, nihil blándit menandri vix ne. Cónsulàtu signíferumque et duo, purtô facer pri ât. Ut quo quis dõlôré ócúrreret. Éà magna alienum has, êi méa graecis perfectó. Dictà dicunt pro id.', 3, '2015-04-18 17:22:25', '2015-04-18 17:22:25', 1, 2, 2, NULL, NULL),
(12, 1, 'Cónsulàtu signíferumque', 'Ut noster quaeréndum has. Pósteá mollis probatús nam in, erát erânt aperiri éi êos, eúm ãn persius nominavi sadipscing. Id per legêre vivendum võlutpat, requê vólumus tôrquátos an sêd, éros sale cu eum. Et vim deseruisse interessét, dictã íntégre usú et. Ei nostro sapíéntem torquãtos cum. Atqúi pôpulo çóncludaturqúe êá mea.\r\n\r\n Decorê principes déterrúisset vim ad, ius no pêrtinãx concludátúrqúe, eum in luptatum perséçúti dissentiunt. Luptãtúm repúdiárê concludáturque mei ei, assúm sónêt qualisque âd çum. No muçius fábulás detrâxit qúo, pri àd legendôs tâcimàtês. Àpériám âccusàm déseruisse id nec, éi modus erípuit fúíssét has. Gràeçi impedit fastídii pri cu, meí regíónê nôstrud âdolesçens éx, vix câsé périculis ut.\r\n\r\n Volúmús oportere expéténdis ét nàm, has melius nusquám eligéndi ei. Ipsúm veritus his et, ádhuc dicànt semper víx id, ênim neglêgéntur tê ius. Nõvum tôrquatos prõ ut, illum virís témpor éa cum, ne ridêns alienum sâlútatus eâm. Eôs at error prômpta çôrrúmpit, ut sed rêquê tritání disputatiôni. Êrrém qúãéstíô eloquéntíam at vim.\r\n\r\n Sea nô quodsi partíendo, nihil blándit menandri vix ne. Cónsulàtu signíferumque et duo, purtô facer pri ât. Ut quo quis dõlôré ócúrreret. Éà magna alienum has, êi méa graecis perfectó. Dictà dicunt pro id.', 1, '2015-04-18 17:34:32', '2015-04-18 17:34:32', 4, 2, 1, NULL, NULL),
(13, 1, ' Inaní íriure', 'Quod diçeret lucilíus ei vix. Ipsum placerât voluptatum vím eu, eum at liber vócênt épícuri, sónet meliús vim ut. Pró te consêtetur philósophià, dôlór quaeque maíestátis qúo in. Nãm fierént râtíóníbus inçidérint éa, est án tota volumus probátus, nó tãle dicunt dolorêm mel. Novum dênique tôrquátos dúô né, êvertitur cónstituam eõs ut. Ut mnésàrçhum êlàbõràret sit. Sit solúta cêtêrõ êuísmõd in, fierent epicurei est eú.\r\n\r\n Nec ex dõlóré qúôdsí êfficíendi, méi équidém fórênsibus et. Usu tâmquam dólôrêm vólútpât âd, ornàtús spléndide cõnçludãturqúe cum nô, hàs te rebum velit. Sêa tõllit abhorreant no, áppétere vúlpútàte liberàvísse vel éx, eu sumó case eos. Vim àlbucius eláborâret ád, mêí éx súmmó nômínaví. Sed mundí tórquãtós voluptãria ín, ut sénsibús eleifend fôrénsibús vix. Verterem tôrquatôs demõcrítúm vél ut, mel an díscerê álbuçius êxpetendis, víx ut âffért intellegebat.\r\n\r\n Àúdíre tâcímates ân vis. Nó íus iracundiá tincidunt, id ãntíopâm áçcommôdaré vix, similíqué àbhõrreant út mei. Cú vim verterem díspútândó intellegebat. Has té veri phílosophíà, ex est omnium civibus. Eú vél fãceté deseruíssê cónsectetúér, quem primis sit eã.\r\n\r\n Errem díçam sed íd, ad equidém príncipes nàm. Àn grâecis invenire êvêrtitur eâm. Id sed àccusamus úrbánitâs, nê vím sônét quõdsi animâl. Nam sonét efficiàntur éx, in nobis vocibús quo.', 1, '2015-04-27 00:37:47', '2015-04-27 00:37:47', 4, 2, 2, NULL, NULL),
(14, 1, ' Àúdíre tâcímates ân vis. Nó íus iracundiá tincidunt, id ãntíopâm áçcommôdaré vix, similíqué àbhõrreant út mei. Cú vim verterem díspútândó intellegebat.', ' Duô zríl eírmod intellegãm té, décôre ancíllae no êam, qúi tritâni võlumus périçúlis út. Vís ex fugit prómpta võluptatum, laudêm fábéllas pró te, te próbo aliquip àltêrum pri. Enim agàm sâlutandi meà at, ne totá salé mei, ne doçtus impetus mel. Ut élit nostrum eúm, qúodsi singulís definitionés cú pér. Corpora pônderúm an duo, vis íd ãliquâm làborãmus ràtionibús. Inání diçam medíôcrem eum at, pro núllam pérícula ne.\r\n\r\n Vítàê ínvéniré eôs te, ãn quí ãppâreàt eurípidis interêsset, dolor munere accommodare êõs in. Vel âssúm aliqúãndô ei. Susçipít plâcerât irâcundia cúm ãn, te augue epícúri vel. Duo ín natúm causáe cõmmodô. Affért legendos àdipisçing éi usu, ei cópiõsaê atomorum constituto quí.\r\n\r\n Ãd mãiõrum singulis nêcessitatíbús êum. Àssueverit scriptorém nó nam, sit pericula sententiae id. Éôs no áutém alterum íntêllêgât, duô quis legendos né. Àn hâs quandó áliquíd blandít, qúí cu vidêrer võluptátum, máíórum instructíór eàm àd. Óptíõn tritáni cõpiosáe éám id, quo àd vocibús nólúisse perfecto. Mei in áetérno quâerendúm, qúo êu móllis ãlíquándô.\r\n\r\n Inaní íriure corpora tê quó, wísi çônsulátu íd sed, est brutê prôpriaé id. Nê íús ménandrí concéptam rêformidàns, cum in erõs delicãtissimí. Iríure intégre commune cu vim. Ex per utrôqué mêntítum similiquê, his in sólet víderer vóluptua, sea dicit décore nê. Àt çibõ blândit salútandi usu. Popúló dissentiét ad ius, dêbêt ornátús scriptôrém nô mêl.', 2, '2015-04-28 18:12:13', '2015-04-28 18:12:13', 4, 2, 2, NULL, NULL),
(17, 1, 'Noticia', 'extra', 1, '2015-05-23 03:16:30', '2015-05-23 03:16:30', 1, 5, 1, NULL, NULL),
(18, 7, 'limao', 'ffffffffffffffffffffff', 1, '2015-05-23 03:22:54', '2015-05-23 03:22:54', 6, 2, 2, NULL, NULL),
(19, 7, 'The Empyrean', 'Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men Lorem ipsum dolor at men ', 4, '2015-05-24 05:06:28', '2015-05-24 05:06:28', 1, 2, 1, NULL, NULL),
(20, 1, 'AVALIAÇÃO MICROBIOLÓGICA DE POLPAS DE FRUTAS CONGELADAS.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 1, '2015-05-25 04:28:57', '2015-05-25 04:28:57', 5, 2, 1, NULL, NULL),
(21, 1, 'A EDUCAÇÃO FÍSICA NO IFAL: AS REPRESENTAÇÕES DOS ALUNOS.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 3, '2015-05-25 04:29:36', '2015-05-25 04:29:36', 6, 5, 6, NULL, NULL),
(22, 7, 'ESTUDOS DE CULTURA MATERIAL, HISTÓRIA DOS ORNAMENTOS ARQUITETÔNICOS DE PENEDO (1613-1822).', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 1, '2013-05-30 13:44:00', '2013-05-30 13:44:00', 7, 5, 3, NULL, NULL),
(23, 7, 'FACHADAS DO CASARIO DE PIRANHAS.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nLorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.', 1, '2013-08-30 13:47:07', '2013-08-30 13:47:07', 8, 5, 2, NULL, NULL),
(24, 7, 'Vel tota detracto et', 'Lorem ipsum dolor sit amet, eu est nibh delicatissimi, pri in oblique partiendo eloquentiam. Mei ne reque vivendum tractatos, eum at probo phaedrum, veritus graecis id vis. Vis agam unum necessitatibus te, integre labitur dolorem no quo. Vim essent neglegentur ad, eam possit dolorem efficiendi cu, ius falli percipit elaboraret te.\r\n\r\n Vel tota detracto et. Vis utamur commune repudiare cu. Mel stet fabulas an, aliquid qualisque usu at. Quo legimus disputando ne, dicit homero in eos, te quo quodsi noluisse lucilius. Mea an persius nominavi, impedit recteque convenire ad sea.\r\n\r\n Est alii regione no, sit no mollis scripta. Sea et quodsi dolorum copiosae, id pro ornatus accusata. Ei quem idque his, perfecto iudicabit instructior nam ad. Ea dicit accusam per, esse essent mollis duo eu. In duo amet fastidii, at nullam partiendo tincidunt mei, legendos argumentum sed ne. Discere fierent philosophia qui at.\r\n\r\n Ius eu malorum inciderint, malorum atomorum vis ei. Vidit vituperatoribus pri at, albucius reprimique cotidieque id eos. Fugit ignota omittantur ne sit. Modus molestiae cu nec. Eam melius denique te. Pro nonumes deserunt ne.', 1, '2015-05-30 16:32:36', '2015-05-30 16:32:36', 9, 2, 1, NULL, NULL),
(25, 7, ' At labores scaevola scribentur eum', ' At labores scaevola scribentur eum, in alia recusabo vix. Vix soluta aliquam quaerendum ex. Has an atomorum disputationi, per malorum labores ceteros ad. Et homero mollis diceret duo, his amet modo graeco ad. Pri ut mutat latine referrentur, te wisi gloriatur mea. Facer malorum ne eos, duo at error invenire.\r\n\r\n Te pro lobortis eloquentiam, eu vero eius etiam sit. Suas autem rationibus nec ei, vel amet elitr everti cu, cibo copiosae principes his no. Ne sea labitur complectitur, eu vocibus omnesque vim. Sed copiosae consulatu definitionem in, euismod fastidii vim ex, vis at laoreet philosophia. Ad sit verear complectitur, modo assum oportere cu sit.\r\n\r\n Fugit necessitatibus eum ne. Ei tale modus nonumes ius. Vix cu iisque reprehendunt, at sea graeco dissentias interpretaris. Mei modus reque et, no vix feugait legendos evertitur. Cum habeo suavitate at, ne est mutat causae aperiri.\r\n\r\n Delectus salutandi scribentur sit an, natum possit cu mel. Ut sea omnes meliore sadipscing, ea quodsi platonem pro, eu possim splendide voluptatum qui. Ne harum assueverit sit, vel habeo oratio recusabo te. Eos ad mucius lobortis, ut cetero suavitate sea, pri an nibh ancillae.', 1, '2015-05-30 16:35:45', '2015-05-30 16:35:45', 5, 2, 1, NULL, NULL),
(26, 1, 'Ne sonet iracundia vel, ei usu timeam adipisci corrumpit', 'Ne sonet iracundia vel, ei usu timeam adipisci corrumpit. Persius fabellas antiopam eu mei, id feugiat delectus vis, in vis etiam zril posidonium. Est prima vivendo omnesque no, dicat diceret no vel, movet hendrerit cu per. Posse docendi fabellas at mel. Illum audiam te mea, at nec putant persequeris. Cu iisque delectus concludaturque qui.\r\n\r\n Nostro eloquentiam qui an, quo delenit senserit cu. Cum sale noluisse ut, at iusto saepe deseruisse eam. Ea mea suas deseruisse, his id impetus prodesset reprimique. Probo accusata eu vel, est no exerci iisque conclusionemque, ea consul euripidis omittantur nam. Mel et facete sanctus veritus, an quando primis quaeque has. Quo ad mediocritatem necessitatibus.\r\n\r\n Eu mea corrumpit prodesset. No case quaerendum necessitatibus ius, atqui oporteat tacimates eum at, ad tantas utroque vis. Movet recteque cu eam, ex vel partem persecuti. Unum iudico pri in, clita oblique ea per. Sumo instructior vim no, sea dicta epicuri in. Ut vis case doctus.\r\n\r\n At labores scaevola scribentur eum, in alia recusabo vix. Vix soluta aliquam quaerendum ex. Has an atomorum disputationi, per malorum labores ceteros ad. Et homero mollis diceret duo, his amet modo graeco ad. Pri ut mutat latine referrentur, te wisi gloriatur mea. Facer malorum ne eos, duo at error invenire.', 4, '2015-07-04 00:43:18', '2015-07-04 00:43:18', 1, 5, 1, NULL, NULL),
(27, 1, ' Choro regione ei quo, ut diam ignota torquatos pro', ' Choro regione ei quo, ut diam ignota torquatos pro. Melius copiosae eam id. Tation iriure lobortis ne mei. Illud iudicabit torquatos ad duo.\r\n\r\n Eam quando inermis in. Eius vivendum vis no, doctus delicatissimi mei no, vix eu invenire pericula referrentur. Vel an intellegat adipiscing posidonium, ex nec enim causae eripuit, id graece ponderum usu. Per ne ludus tempor habemus. Eleifend principes ad pri, veniam dolore ut pri, mea salutatus philosophia ut.\r\n\r\n Dicant tritani id ius. Vix no elit conclusionemque. Facilisi molestiae prodesset has te. Sed te ludus invidunt, id eum omnes nonumy apeirian. Vel assum decore reformidans id, ea mea facete civibus, impetus nominavi in vel. Et pri cibo consul, putant everti cu usu.\r\n\r\n Eros aliquid te vim, nec in illum diceret prodesset, pri ea accusamus euripidis disputando. Commune ocurreret per in. Mel oporteat liberavisse at, te eius saepe ocurreret sed. Mea ornatus temporibus concludaturque in, duis rebum efficiantur sed eu, dolorem volumus pri eu.\r\n\r\n Id audire erroribus dissentiunt sit. Usu ne admodum electram, ad mel tota simul choro, omnesque menandri eloquentiam vis in. Vulputate definitiones nec in. Nec ea regione similique, decore intellegam appellantur vim no, ea eos dissentias inciderint.', 4, '2015-07-05 00:40:10', '2015-07-05 00:40:10', 6, 5, 1, NULL, NULL),
(28, 7, 'Quaeque iudicabit honestatis mea a', 'Quaeque iudicabit honestatis mea an, ut posse homero disputationi qui, te sit feugait definiebas. Id sea maiorum offendit, ad suas veri reformidans sed. Te autem postea numquam his, qui exerci epicurei id. Omnium blandit iracundia mea te, ea nihil commune sea, no sed mucius appellantur. Ne duis nonumy definiebas est, epicuri invenire laboramus an has. At vim voluptaria philosophia. Stet viris vulputate in pro, has ex petentium patrioque voluptatibus.\r\n\r\n Choro regione ei quo, ut diam ignota torquatos pro. Melius copiosae eam id. Tation iriure lobortis ne mei. Illud iudicabit torquatos ad duo.\r\n\r\n Eam quando inermis in. Eius vivendum vis no, doctus delicatissimi mei no, vix eu invenire pericula referrentur. Vel an intellegat adipiscing posidonium, ex nec enim causae eripuit, id graece ponderum usu. Per ne ludus tempor habemus. Eleifend principes ad pri, veniam dolore ut pri, mea salutatus philosophia ut.', 4, '2015-07-05 00:45:09', '2015-07-05 00:45:09', 1, 5, 1, NULL, NULL),
(29, 7, 'Esse tôllit théóphrastus sit cu, cãúsãe pérfectó', ' Inaní íriure corpora tê quó, wísi çônsulátu íd sed, est brutê prôpriaé id. Nê íús ménandrí concéptam rêformidàns, cum in erõs delicãtissimí. Iríure intégre commune cu vim. Ex per utrôqué mêntítum similiquê, his in sólet víderer vóluptua, sea dicit décore nê. Àt çibõ blândit salútandi usu. Popúló dissentiét ad ius, dêbêt ornátús scriptôrém nô mêl.\r\n\r\n Esse tôllit théóphrastus sit cu, cãúsãe pérfectó ex séd, pericúla tractátós vís cu. Vix diám áliquam eu, ãlía nobís nusquam ea vim. Veritús désêrunt àn pêr. Tàtion sémper at mél, nãtum dôlorê aliquandô àd ius. Cibo ápêriri integre vim et.\r\n\r\n Quod diçeret lucilíus ei vix. Ipsum placerât voluptatum vím eu, eum at liber vócênt épícuri, sónet meliús vim ut. Pró te consêtetur philósophià, dôlór quaeque maíestátis qúo in. Nãm fierént râtíóníbus inçidérint éa, est án tota volumus probátus, nó tãle dicunt dolorêm mel. Novum dênique tôrquátos dúô né, êvertitur cónstituam eõs ut. Ut mnésàrçhum êlàbõràret sit. Sit solúta cêtêrõ êuísmõd in, fierent epicurei est eú.', 1, '2015-07-05 02:30:30', '2015-07-05 02:30:30', 3, 5, 2, NULL, NULL),
(30, 7, 'TRATAMENTO DE ÁGUA CONTAMINADA COM GASOLINA UTILIZANDO O RESÍDUO DA PRODUÇÃO DE BIOETANOL 2G (LIGNINA) A PARTIR DOS RESÍDUOS FIBROSOS DA AGROINDÚSTRIA DE FRUTICULTURA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2015-07-14 18:09:44', 1, 3, 1, NULL, NULL),
(31, 7, 'TRATAMENTO DE ÁGUA CONTAMINADA COM GASOLINA UTILIZANDO OS RESÍDUOS FIBROSOS, RICOS EM CELULOSE DA AGROINDÚSTRIA DE FRUTICULTURA.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 2, 1, NULL, NULL),
(32, 7, 'ANÁLISE DOS EFEITOS DA TEMPERATURA DE SECAGEM NAS PROPRIEDADES MECÂNICAS DE NANOCOMPÓSITOS POLIMÉRICOS BIODEGRADÁVEIS PRODUZIDOS COM CARBOXIMETILCELULOSE, POLIETILENO GLICOL E ARGILAS MINERAIS.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 3, 2, NULL, NULL),
(33, 7, 'O LEITE DE CABRA NA FORMULAÇÃO DE NOVOS COSMÉTICOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 11, NULL, NULL),
(34, 7, 'DESENVOLVIMENTO DE INDICADORES NATURAIS A BASE DE EXTRATO VEGETAIS RICOS EM ANTOCIANINAS E BETALAINAS PARA AVALIAÇÃO DO pH DE LEITE CAPRINO E BOVINO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 3, 11, NULL, NULL),
(35, 7, 'DESENVOLVIMENTO DE QUEIJO DE COALHO LIGHT COM AROMA DE MANTEIGA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 11, NULL, NULL),
(36, 7, 'DESENVOLVIMENTO DE BOLO DIET RICO EM FIBRAS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 3, 11, NULL, NULL),
(37, 7, 'SÍNTESE DE ESFERAS DE LÁTEX COM APLICAÇÃO NA FORMAÇÃO DE MATERIAIS POROSOS ORDENADOS PARA CELULAS E DESALINIZADORES', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 2, NULL, NULL),
(38, 7, 'HEMOBANK: UM APLICATIVO MOBILE PARA ACOMPANHAMENTO DE BANCOS DE SANGUE E DOAÇÕES PARA A HEMORREDE DE ALAGOAS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 4, 1, NULL, NULL),
(39, 7, 'DESENVOLVIMENTO DO GEORREFERENCIAMENTO DOS ATENDIMENTOS DO SAMU 192 DA CENTRAL REGIONAL DE MACEIÓ-AL', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 1, NULL, NULL),
(40, 7, 'DESENVOLVIMENTO DE APLICAÇÕES EDUCATIVAS PARA TV-DIGITAL INTERATIVA COM NCL/LUA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 4, 3, NULL, NULL),
(41, 7, 'ROTAUT: UM JOGO PARA AUXILIAR NA APRENDIZAGEM DA ROTINA DA CRIANÇA AUTISTA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 1, NULL, NULL),
(42, 7, 'PROTÓTIPO DE UM APLICATIVO DE JOGO DA FORCA PARA PESSOAS COM AUTISMO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 1, NULL, NULL);
INSERT INTO `projects` (`id`, `user_id`, `title`, `abstract`, `status_id`, `created`, `modified`, `area_id`, `intellectual_property_id`, `campus_id`, `mermbers_list_id`, `date`) VALUES
(43, 7, 'SÍNTESE E CARACTERIZAÇÃO DE NOVAS PLATAFORMAS FUNDAMENTADA NA TECNOLOGIA DE IMPRESSÃO MOLECULAR E SEU USO NA CONSTRUÇÃO DE NOVOS SENSORES QUÍMICOS PARA MONITORAMENTO DE ESPÉCIES DE INTERESSE AMBIENTAL', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 4, 1, NULL, NULL),
(44, 7, 'FOLLOW-ME: MONITORAMENTO DE INSTALAÇÕES UTILIZANDO VANTS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 4, 6, NULL, NULL),
(45, 7, 'SMARTLIVING: UMA SOLUÇÃO DE AUTOMAÇÃO RESIDENCIAL DE BAIXO CUSTO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 6, NULL, NULL),
(46, 7, 'SAT - SERVIÇO DE APOIO AO TAXISTA - PARTE 2', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 1, 1, 1, NULL, NULL),
(47, 7, 'COMPOSTAGEM DE RESÍDUOS VEGETAIS ENRIQUECIDOS COM URINA HUMANA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 11, NULL, NULL),
(48, 7, 'PLANTAS MEDICINAIS UTILIZADAS EM ÁREAS DE ASSENTAMENTOS RURAIS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 4, 12, NULL, NULL),
(49, 7, 'APROVEITAMENTO DE MATERIAIS ORGÃNICOS E INORGÃNICOS PROVENIENTES DA FEIRA LIVRE DE MARAGOGI', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 12, NULL, NULL),
(50, 7, 'ELABORAÇÃO DE IOGURTE ENRIQUECIDO COM POLPA DE SAPOTI (manilkara zapota l.)', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 4, 11, NULL, NULL),
(51, 7, 'DESENVOLVIMENTO DE CREME DE RICOTA CONDIMENTADO COM PIMENTA ROSA (Schinus molle l.)', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 4, 11, NULL, NULL),
(52, 7, 'TEOR DE AGROTÓXICO NA MANTEIGA BOVINA COMO INDICADOR DE CONTAMINAÇÃO DE ÁREA DE PASTEJO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 11, NULL, NULL),
(53, 7, 'CONCHAS DE OSTRA (Crassostrea rhizophorae) COMO FONTE ALTERNATIVA DE CÁLCIO EM DIETAS DE TILÁPIAS DO NILO (OREOCHROMIS NILOTICUS)', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 11, NULL, NULL),
(54, 7, 'ESTUDO DE COPRODUTOS DO BENEFICIAMENTO DE FRUTAS VISANDO A PRODUÇÃO DE INGREDIENTES PARA RAÇÃO DE PEIXES', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 11, NULL, NULL),
(55, 7, 'AVALIAÇÃO DAS CARACTERÍSTICAS AGRONÔMICAS DE DUAS LEGUMINOSAS ARBUSTIVAS NATIVAS PARA FINS DE ADUBO VERDE', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 5, 1, 12, NULL, NULL),
(56, 7, 'PRODUÇÃO DE BIOADUBO PELO PROCESSO DE VERMICOMPOSTAGEM DO BAGAÇO DE LARANJA LIMA', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 2, 4, 5, NULL, NULL),
(57, 7, 'ESTUDO DE CULTURAS E SUAS ADAPTAÇÕES EM SOLOS SEMI-ÁRIDOS DO AGRESTE ALAGOANO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 2, 1, 6, NULL, NULL),
(58, 7, 'DESENVOLVIMENTO DE INSTRUMENTO DE AVALIAÇÃO DA ESTIMATIVA PERCEPTOMOTORA UTILIZANDO COMPUTAÇÃO PERVASIVA.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 6, 1, 6, NULL, NULL),
(59, 7, 'DESENVOLVIMENTO DE ESTIMULADOR TRANSCRANIANO POR CORRENTE CONTINUA AUTOMATIZADO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 6, 1, 6, NULL, NULL),
(60, 7, 'PERFIL DEMOGRÁFICO E SOCIOECONOMICO DE ESTUDANTES DA MODALIDADE PROEJA NO INSTITUTO FEDERAL DE ALAGOAS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 6, 1, 2, NULL, NULL),
(61, 7, 'ACESSIBILIDADE UM DESAFIO PARA SUSTENTABILIDADE', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 8, 4, 7, NULL, NULL),
(62, 7, 'LEVANTAMENTO DO SABER-FAZER DO QUEIJO DE MANTEIGA EM ALAGOAS: EM BUSCA DA IDENTIDADE CULTURAL', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 8, 1, 11, NULL, NULL),
(63, 7, 'JOGOS ELETRÔNICOS COM FINS EDUCACIONAIS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 4, 1, 6, NULL, NULL),
(64, 7, 'AMBIENTE DE APRENDIZADO AOTOCONTROLADO', '', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 4, 1, 6, NULL, NULL),
(65, 7, 'INSERÇÃO DO SORVETE À BASE DE LEGUMES NA ALIMENTAÇÃO ESCOLAR.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 4, 1, 1, NULL, NULL),
(66, 7, 'DESENVOLVIMENTO DE UM JOGO EDUCATIVO BASEADO EM CARTAS DE BARALHO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 4, 1, 2, NULL, NULL),
(67, 7, 'DESIGN E INOVAÇÃO NA PRODUÇÃO DE ESTOFADOS PERSONALIZADOS: IDEAÇÃO E EXPERIMENTAÇÃO A PARTIR DA REUTILIZAÇÃO DE SUBPRODUTOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL),
(68, 7, 'ANÁLISE DA DURABILIDADE MEDIANTE INCORPORAÇÃO DE RESÍDUO DE CERÂMICA VERMELHA DE OLARIAS DE ALAGOAS EM CONCRETO', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 6, NULL, NULL),
(69, 7, 'SOFTWARE DE SIMULAÇÃO DO ARRANJO DO EMPACOTAMENTO DE PARTÍCULAS E OS VAZIOS EXISTENTES', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 6, NULL, NULL),
(70, 7, 'CONTROLE REMOTO DA TEMPERATURA DE EQUIPAMENTOS ELÉTRICOS, APLICANDO MICROONTROLADORES EM CELULARES DOMÉSTICOS.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 3, NULL, NULL),
(71, 7, 'INVESTIGAÇÃO DA RESISTÊNCIA À COMPRESSÃO E À TRAÇÃO EM CONCRETOS COM ADIÇÃO DE UM BIOPOLÍMERO.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL);
INSERT INTO `projects` (`id`, `user_id`, `title`, `abstract`, `status_id`, `created`, `modified`, `area_id`, `intellectual_property_id`, `campus_id`, `mermbers_list_id`, `date`) VALUES
(72, 7, 'MEDIDOR DE POTENCIAL EÓLICO PARA MICROGERAÇÃO EM EDIFICAÇÕES URBANAS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL),
(73, 7, 'ESTUDO SOBRE INDICADORES DE DESEMPENHO OPERACIONAL RELACIONADOS AO CONSUMO DE ÁGUA E GERAÇÃO DE EFLUENTES EM UMA FÁBRICA PRODUTORA DE DERIVADOS DO LEITE EM PALMEIRA DOS ÍNDIOS, AL.', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 6, NULL, NULL),
(74, 7, 'INTRODUÇÃO AO ESTUDO DOS MATERIAIS COMPÓSITOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL),
(75, 7, 'O PROBLEMA DA INCLUSÃO EQUIVALENTE DE ESHELBY PARA MATERIAIS COMPÓSITOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 1, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL),
(76, 7, 'MODELOS MICROMECÂNICOS PARA ANÁLISE DE MATERIAS COMPÓSITOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 1, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 1, NULL, NULL),
(77, 7, 'ESTUDO DA VIABILIDADE DO USO DAS CINZAS DA CASCA DO ARROZ PARA FABRICAÇÃO DE BLOCOS VAZADOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 4, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 6, NULL, NULL),
(78, 7, 'ANÁLISE DO COMPORTAMENTO DA BORRACHA DE PNEUS INSERVÍVEIS NA FABRICAÇÃO DE PISOS', 'Lorem ipsum dolor sit amet, ut est tollit tamquam, commodo aliquip eam no. Vis et nobis dicunt. Libris regione intellegebat est at, mea officiis imperdiet no. Vim ea homero feugiat, qui ad erat periculis democritum, no viderer molestiae vulputate nam. Id civibus efficiendi eos, scaevola gubergren neglegentur quo in. Vim ut feugiat facilis reformidans, laoreet suscipit delicata ex vel.\r\n\r\nEst amet postea te, pri ne erat verterem iudicabit, cu has alia eripuit commune. Vis cu integre constituto vituperatoribus, ferri exerci posidonium eu per. Usu no detracto invidunt perfecto, his persius phaedrum scripserit eu. Simul hendrerit sententiae per ea.\r\n\r\nDelicata erroribus voluptatibus quo ei, viris oblique duo eu. Id vim tota facilisi, et altera dolores antiopam mea. Malis scripserit an sit, eu eum justo offendit aliquando. Qui solet audiam timeam ut, prompta lucilius delicatissimi ei quo, cu mazim tincidunt sit. Velit oblique pri te.\r\n\r\nHabeo posidonium ex vim. Saepe ornatus ius ei. Et facer clita suscipit vix. Suas consulatu in est, te adhuc idque feugiat ius. Has summo error ex, in decore nostro has.\r\n\r\nIn magna percipitur vix, explicari maluisset vix ad, at natum sadipscing dissentiet nam. Vis aliquam eleifend id. Graeci laoreet has no, vel et paulo officiis scriptorem. An mel iudico aliquid senserit, ne discere similique duo, ei mucius scaevola adipiscing vel. Inani integre quo ut, homero lucilius at nam, choro iuvaret te vim. Mea et idque dolorum, cu utamur recusabo mei, usu et gloriatur signiferumque. Autem utinam indoctum mel eu, ex viderer assentior eos.', 1, '2014-08-01 00:00:00', '2014-08-01 00:00:00', 3, 1, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `searches`
--

CREATE TABLE IF NOT EXISTS `searches` (
`id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` int(12) NOT NULL DEFAULT '1',
  `finished` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `searches`
--

INSERT INTO `searches` (`id`, `user_id`, `project_id`, `created`, `modified`, `status_id`, `finished`) VALUES
(8, 1, 10, '2015-04-18 19:03:37', '2015-04-18 19:03:37', 3, 0),
(12, 1, 14, '2015-04-28 18:43:22', '2015-04-28 18:43:22', 1, 0),
(18, 1, 12, '2015-05-20 05:12:21', '2015-05-20 05:12:21', 1, 0),
(20, 5, 13, '2015-05-20 05:15:07', '2015-05-20 05:15:07', 1, 0),
(23, 1, 18, '2015-05-23 03:34:18', '2015-05-23 03:34:18', 1, 0),
(27, 1, 17, '2015-05-24 03:28:37', '2015-05-24 03:28:37', 1, 0),
(28, 7, 21, '2015-05-25 17:55:19', '2015-05-25 17:55:19', 3, 0),
(35, 7, 9, '2015-05-26 20:13:43', '2015-05-26 20:13:43', 3, 0),
(36, 1, 20, '2015-05-29 02:32:53', '2015-05-29 02:32:53', 1, 0),
(37, 7, 24, '2015-07-04 02:39:27', '2015-07-04 02:39:27', 1, 0),
(41, 7, 29, '2015-07-05 20:23:17', '2015-07-05 20:23:17', 1, 0),
(42, 9, 23, '2015-07-06 21:16:34', '2015-07-06 21:16:34', 1, 0),
(43, 8, 25, '2015-07-06 21:17:24', '2015-07-06 21:17:24', 1, 0),
(44, 7, 22, '2015-07-07 20:11:06', '2015-07-07 20:11:06', 1, 0),
(45, 1, 78, '2015-07-14 20:04:38', '2015-07-14 20:04:38', 1, 0),
(47, 7, 76, '2015-07-15 23:41:03', '2015-07-15 23:41:03', 1, 0),
(48, 1, 75, '2015-07-16 00:01:41', '2015-07-16 00:01:41', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE IF NOT EXISTS `status` (
`id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Busca em andamento'),
(4, 'Busca não iniciada'),
(3, 'Nenhum projeto encontrado'),
(2, 'Projeto idêntico encontrado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(10) NOT NULL DEFAULT 'user'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `created`, `modified`, `role`) VALUES
(1, 'leonardokl@hotmail.com', 'Leonardo Luiz', '$2y$10$yZrH35/wOlvuFTIpIIXdTuBwvp/zWn2CH5UfXZB98R87NOTZ.nOZK', '2015-04-17 02:15:55', '2015-04-17 02:15:55', 'user'),
(4, 'ale@email.com', 'Alexandre Ferreira', '$2y$10$PXLikD4B40eca0F.tClEousL8uHL0d1fZgmaE/xeE5usVMezyJHCG', '2015-04-18 03:07:38', '2015-04-18 03:07:38', 'user'),
(5, 'eric@email.com', 'Eric Belarmino Diniz', '$2y$10$Qw/pR3uZtbkvzv4EeVEHFuGm/JeOHd9osqMa/YvqBHcXqutHbJVIq', '2015-04-18 03:08:27', '2015-04-18 03:08:27', 'user'),
(6, 'jairo@email.com', 'Jairo José', '$2y$10$zV89d9Eq8xou1d2VUZvlyeCji75VY9Mo5cKfbySaoAe8kSwW15sGK', '2015-05-19 22:51:00', '2015-05-19 22:51:00', 'user'),
(7, 'admin@email.com', 'Admin', '$2y$10$FBFC1cijjgBKgIYl/dl1tuGvMG8TSguTGUI2LO2to6x2VdVM90UQq', '2015-05-19 23:02:34', '2015-05-19 23:02:34', 'admin'),
(8, 'diego@email.com', 'Diego Rodrigues', '$2y$10$qdT/EBToBRvqkOkkq4jJDeBYLm.SIwCbT2b0hXybPB8mRIUDD7ahC', '2015-05-24 06:32:59', '2015-05-24 06:32:59', 'user'),
(9, 'Kilary@email.com', 'Kilary Fernandes', '$2y$10$aN25ek6MQ6CjDsTCocM5WurHLO59PCX42Ia3brGBLg4wnptLsciAS', '2015-05-24 06:36:07', '2015-05-24 06:36:07', 'user'),
(10, 'tiago@email.com', 'Tiago', '$2y$10$gGZhssai9Gsq3VNj60GDlOJ7DtBfIS5UA8hwUycc22/NmAff2smf2', '2015-07-16 00:05:10', '2015-07-16 00:05:10', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campus`
--
ALTER TABLE `campus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
 ADD PRIMARY KEY (`id`), ADD KEY `descriptions_list_id` (`search_id`);

--
-- Indexes for table `intellectual_properties`
--
ALTER TABLE `intellectual_properties`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`), ADD KEY `members_status_id` (`members_status_id`), ADD KEY `campus_id` (`campus_id`), ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `members_lists`
--
ALTER TABLE `members_lists`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `project_id` (`project_id`);

--
-- Indexes for table `members_status`
--
ALTER TABLE `members_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notifications_lists`
--
ALTER TABLE `notifications_lists`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`), ADD KEY `user_id_2` (`user_id`), ADD KEY `user_id_3` (`user_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `search_id` (`status_id`,`mermbers_list_id`), ADD KEY `mermbers_list_id` (`mermbers_list_id`), ADD KEY `campus_id` (`campus_id`), ADD KEY `intellectual_property_id` (`intellectual_property_id`), ADD KEY `area_id` (`area_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `searches`
--
ALTER TABLE `searches`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `project_id` (`project_id`), ADD KEY `status_id` (`status_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `campus`
--
ALTER TABLE `campus`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `descriptions`
--
ALTER TABLE `descriptions`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `intellectual_properties`
--
ALTER TABLE `intellectual_properties`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `members_lists`
--
ALTER TABLE `members_lists`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `members_status`
--
ALTER TABLE `members_status`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `notifications_lists`
--
ALTER TABLE `notifications_lists`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `searches`
--
ALTER TABLE `searches`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `descriptions`
--
ALTER TABLE `descriptions`
ADD CONSTRAINT `descriptions_ibfk_1` FOREIGN KEY (`search_id`) REFERENCES `searches` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `members`
--
ALTER TABLE `members`
ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `members_ibfk_3` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `members_lists`
--
ALTER TABLE `members_lists`
ADD CONSTRAINT `members_lists_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `notifications`
--
ALTER TABLE `notifications`
ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `notifications_lists`
--
ALTER TABLE `notifications_lists`
ADD CONSTRAINT `notifications_lists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `projects`
--
ALTER TABLE `projects`
ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`area_id`) REFERENCES `areas` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `projects_ibfk_3` FOREIGN KEY (`intellectual_property_id`) REFERENCES `intellectual_properties` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `projects_ibfk_4` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `projects_ibfk_5` FOREIGN KEY (`mermbers_list_id`) REFERENCES `members_lists` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `searches`
--
ALTER TABLE `searches`
ADD CONSTRAINT `searches_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `searches_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `searches_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
