<?php

use Phinx\Migration\AbstractMigration;

class CreatePosts extends AbstractMigration
{
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table("posts");
        $table->addColumn('title', 'string')
        ->addColumn('body', 'text')
        ->addColumn('created', 'datetime')
        ->addColumn('modified', 'datetime')
        ->save(); 
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        if($this->hasTable("posts")) {
            $this->dropTable("posts");
        }
    }
}