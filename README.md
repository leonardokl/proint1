## Requisitos
1. PHP 5.6.3
1. Composer - https://getcomposer.org/
2. Git - http://git-scm.com/

## Instalação
1. Clonar o repositório utilizando o git;
2. Habilite a extensão intl no arquivo php.ini do apache. Para habilita-lá vc deve descomentar a linha "extension=php_intl.dll"; 
3. Descomente o mod_rewrite do apache no arquivo httpd.conf. "LoadModule rewrite_module modules/mod_rewrite.so";
4. Ainda no httpd.confi habilite o allow override na tag Directory, substituindo o none por all: AllowOverride All
5. Executar o comando "composer update" na pasta principal do projeto, para que as dependências sejam instaladas.
Configure o banco na pasta "config/app.php"