<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DescriptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DescriptionsTable Test Case
 */
class DescriptionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Descriptions' => 'app.descriptions',
        'Searches' => 'app.searches',
        'Projects' => 'app.projects',
        'Areas' => 'app.areas',
        'Status' => 'app.status',
        'Campus' => 'app.campus',
        'IntellectualProperties' => 'app.intellectual_properties',
        'Users' => 'app.users',
        'Notifications' => 'app.notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Descriptions') ? [] : ['className' => 'App\Model\Table\DescriptionsTable'];
        $this->Descriptions = TableRegistry::get('Descriptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Descriptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
