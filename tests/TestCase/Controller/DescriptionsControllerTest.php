<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DescriptionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DescriptionsController Test Case
 */
class DescriptionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Descriptions' => 'app.descriptions',
        'Searches' => 'app.searches',
        'Projects' => 'app.projects',
        'Areas' => 'app.areas',
        'Status' => 'app.status',
        'Campus' => 'app.campus',
        'IntellectualProperties' => 'app.intellectual_properties',
        'Users' => 'app.users',
        'Notifications' => 'app.notifications'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
